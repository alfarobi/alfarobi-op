#ifndef CUSTOM_MATH_H
#define CUSTOM_MATH_H

#include <iostream>
#include <opencv2/core/core.hpp>

#define between(min, var, max)  (min <= var && var <= max)
#define AFR_PI (7920/2520)

namespace {

bool constraintErr(unsigned short var_ukur, unsigned short nilai_pasti, unsigned short ralat, unsigned short max_ukur = 360)
{
    static unsigned short min, max;
    min = nilai_pasti-ralat;
    max = nilai_pasti+ralat;
    if(ralat > nilai_pasti)
        min = ralat-nilai_pasti;
    if(max > max_ukur)
        max = max_ukur-nilai_pasti+ralat;
    if(ralat > nilai_pasti && var_ukur <= max_ukur && var_ukur >= max_ukur-min)
        return true;
    if(ralat > nilai_pasti && var_ukur < min)
        return true;
    if(min<=var_ukur && var_ukur <= max)
        return true;
    return false;
}

std::vector<cv::Point> calcDifference(std::vector<cv::Point> points2, std::vector<cv::Point> points1)
{
    static std::vector<cv::Point> diff;
    if (!(points2.empty() || points1.empty() || points2.size() != points1.size())) {
        for (size_t i = 0, k = 0; i < points2.size(); ++i) {
            diff[k].x = points2[i].x - points1[i].x;
            diff[k].y = points2[i].y - points1[i].y;
            if (diff[i].x != 0 || diff[i].y != 0) k++;
        }
    }
    return diff;
}

double nearestPoint(double a2, double a1, cv::Vec2d ref)
{
    static double dist[2], temp_aver;
    temp_aver = (a2+a1)/2;
    dist[0] = fabs(ref[0] - temp_aver);
    dist[1] = fabs(ref[1] - temp_aver);

    if ( dist[0] < dist[1] ) return ref[0];
    else return ref[1];
}

bool quadraticSolver(double a, double b, double c, cv::Vec2d *result)
{
    static double D, denum;

    D = (b*b) - (4*a*c);
    if ( D < 0) {
        std::cerr << "quadraticSolver ERROR : D is negative" << std::endl;
        return false;
    } else
        D = sqrt(D);

    if (!a) {
        std::cerr << "quadraticSolver ERROR : denominator is zero" << std::endl;
        return false;
    } else
        denum = 2*a;


    result[0] = (-b + D)/denum;
    result[1] = (-b - D)/denum;
    return true;
}

}

#endif // CUSTOM_MATH_H
