#ifndef SPEED_TEST_H
#define SPEED_TEST_H

#include <sys/time.h>
#include <time.h>
#include <iostream>
#include <stdint.h>

namespace afr {

// ===================== FOR USE ======================================
#define PARENT_TIMER_START  clock_gettime(CLOCK_MONOTONIC, &parent_s)
#define TIME_ELAPSE         timeElapse()
// --------------------------------------------------------------------

static struct timespec pend_s, parent_s;

inline long speedMeasure(const timespec &start_s, const timespec &end_s) {
    static timespec res_s;
    do {
        res_s.tv_sec = end_s.tv_sec - start_s.tv_sec;
        res_s.tv_nsec = end_s.tv_nsec - start_s.tv_nsec;
        if (res_s.tv_nsec < 0) {
            --res_s.tv_sec;
            res_s.tv_nsec += 1000000000;
        }
    } while (0);
    return (res_s.tv_sec + res_s.tv_nsec/(CLOCKS_PER_SEC*1000));
}

inline long speedMeasure(const timespec &start_s) {
    static timespec end_s;
    return speedMeasure(start_s, end_s);
}

inline long timeElapse() {
    clock_gettime(CLOCK_MONOTONIC, &pend_s);
    return speedMeasure(parent_s, pend_s);
}


}

#endif // SPEED_TEST_H
