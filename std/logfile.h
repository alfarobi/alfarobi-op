#ifndef LOGFILE_H
#define LOGFILE_H

#include <fstream>
#include <sstream>
#include <unistd.h>
#include <sys/stat.h>
#include <ctime>

namespace afr {

#define to_string(x) static_cast<std::ostringstream*>( &(std::ostringstream() << x) )->str()

/*!
 * \brief Class for make data logging
 */
class LogFile {

private:
    std::fstream log_file;
    time_t start_time;

public:
    LogFile(const std::string &filename) {
        static bool logging = false;
        static std::string name;

        name = filename;
        if(!logging) {
            time(&start_time);

            name += " ";
            name += ctime(&start_time);
            name += ".log";

            logging = true;
            log_file.open(name.c_str(), std::ios_base::out | std::ios_base::app);
        }
    }

    // WARNING : timing func. Maybe for simple time use difftime()
    void createLine() {
        log_file << std::endl;
        log_file << difftime(time(NULL), start_time) << "\t";
    }

    void logData(const std::string &text) {
        log_file << text;
    }

    void logData(const int &value) {
        log_file << value << "\t";
    }

    void logData(const unsigned int &value) {
        log_file << value << "\t";
    }

    void logData(const long &value) {
        log_file << value << "\t";
    }

    void logData(const double &value) {
        log_file << value << "\t";
    }

    double getdiffTime() {
        static double dt, last_dt, diff_time;

        dt = difftime(time(NULL), start_time);
        diff_time = dt - last_dt;
        last_dt = dt;

        return diff_time;
    }
};

}

#endif // LOGFILE_H
