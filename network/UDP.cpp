/*
 *   UDP.cpp
 *
 *   Author: Alfarobi
 *
 */

#include "UDP.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/ioctl.h>
#include <net/if.h>
#include <ifaddrs.h>

#include <ctype.h>

#include <sstream>
#define to_string(x) static_cast<std::ostringstream*>( &(std::ostringstream() << x) )->str()

using namespace afr;


int UDPSocket::so_optaddr = 1;


UDPSocket::UDPSocket()
{
}

/*!
 * \brief create socket
 * \param address_port
 * \param protocol
 * \sa UDPSocket::create()
 */
UDPSocket::UDPSocket(const char *address_port, char *protocol)
{
    create(address_port, protocol);
}


UDPSocket::~UDPSocket()
{
}

/*!
 * \brief create socket
 * \param address_port
 * \param protocol
 * \return \c true if success
 * \sa UDPSocket::UDPSocket(const char *address_port, char *protocol)
 */
bool UDPSocket::create(const char *address_port, const char *protocol)
{
    /*
    * Create a UDP socket to use:
    */
    s = socket(AF_INET,SOCK_DGRAM,0);
    if ( s == -1 )
        return displayError("socket()");

    /*
    * Form the broadcast address:
    */
    len_inet = sizeof adr;

    int z = mkaddr(&adr,
               &len_inet,
               address_port,
               protocol);

    if ( z == -1 )
        return displayError("Bad broadcast address");
    return true;
}

/*!
 * \brief select option.
 * \param opt \c SO_BROADCAST or \c SO_REUSEADDR
 * \return \c true if success
 */
bool UDPSocket::option(const int &opt)
{
    /*
    * Allow multiple listeners on the
    * broadcast address:
    */
    int z = setsockopt(s,
                   SOL_SOCKET,
                   opt,
                   &so_optaddr,
                   sizeof so_optaddr);

    if ( z == -1 )
        return displayError("setsockopt()");
    return true;
}

/*!
 * \brief bind the socket
 * \return \c true if success
 */
bool UDPSocket::binding()
{
    /*
    * Bind our socket to the broadcast address:
    */
    int z = bind(s,
             (struct sockaddr *)&adr,
             len_inet);

    if ( z == -1 )
        return displayError("bind(2)");
    return true;
}

/*!
 * \brief bind the socket to certain interface
 * \param interface eth0, wlan0, or ppp0
 * \return \c true if success
 * \sa UDPServer
 */
bool UDPSocket::binding(const char *interface)
{
//    static ifreq ifr;
    static ifaddrs *ifadrs, *ifa;

    /*
     * Get the machine address
     */
    int z = getifaddrs(&ifadrs);
    if ( z == -1)
        return displayError("getifaddr(3)");
//    ifr.ifr_addr.sa_family = AF_INET;
//    strncpy(ifr.ifr_name, interface, IFNAMSIZ-1);
//    ioctl(s, SIOCGIFADDR, &ifr);

    /*
    * Bind machine address to our socket, so that
    * client programs can listen to this
    * server:
    */
    for (ifa = ifadrs;  ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr == NULL) continue;
        if (strcmp(ifa->ifa_name, interface) == 0) {
            int z = bind(s,
                     ifa->ifa_addr,
                     sizeof ifa->ifa_addr);
            if ( z == -1 )
                return displayError("bind(2)");
            return true;
        }
    }

    return displayError("interface not connected");
}

/*!
 * \brief recieve data
 * \param data
 * \param length
 * \return \c true if success
 * \sa UDPSocket::send()
 */
bool UDPSocket::recv(void *data, uint length)
{
    int z = recvfrom(s,      /* Socket */
                 data,  /* Receiving buffer */
                 length,/* Max rcv buf size */
                 0,      /* Flags: no options */
                 (struct sockaddr *)&adr, /* Addr */
                 &x);    /* Addr len, in & out */

    if ( z < 0 )
        return displayError("recvfrom(2)"); /* else err */
    return true;
}

/*!
 * \brief receive data
 * \param data
 * \return
 * \sa UDPSocket::send()
 */
bool UDPSocket::recv(std::string & data)
{
    char buf[data.capacity()];

    int z = ::recv(s, buf, data.capacity(), 0);
    if ( z < 0 )
        return displayError("recvfrom(2)"); /* else err */
    data = buf;
    return true;
}

/*!
 * \brief send data
 * \param data
 * \param length
 * \return \c true if success
 * \sa UDPSocket::recv()
 */
bool UDPSocket::send(const void *data, uint length) const
{
    int z = sendto(s,      /* Socket */
               data,  /* Receiving buffer */
               length,/* Max rcv buf size */
               0,      /* Flags: no options */
               (const struct sockaddr *)&adr, /* Addr */
               sizeof(adr));    /* Addr len, in & out */

    if ( z < 0 )
        return displayError("recvfrom(2)"); /* else err */
    return true;
}

/*!
 * \brief send data
 * \param data
 * \return \c true if success
 * \sa UDPSocket::recv()
 */
bool UDPSocket::send(const std::string data) const
{
    int z = ::send(s, data.c_str(), data.length(), MSG_NOSIGNAL);
    if ( z < 0 )
        return displayError("recvfrom(2)"); /* else err */
    return true;
}


int UDPSocket::mkaddr(void *addr, int *addrlen, const char *str_addr, const char *protocol)
{
    char *inp_addr = strdup(str_addr);
    char *host_part = strtok(inp_addr, ":" );
    char *port_part = strtok(NULL, "\n" );
    struct sockaddr_in *ap =
            (struct sockaddr_in *) addr;
            struct hostent *hp = NULL;
            struct servent *sp = NULL;
            char *cp;
            long lv;

            /*
    * Initialize the address structure:
    */
            memset(ap,0,*addrlen);
            ap->sin_family = AF_INET;
            ap->sin_port = 0;
            ap->sin_addr.s_addr = INADDR_ANY;

            /*
    * Fill in the host address:
    */
            if ( strcmp(host_part, "*" ) == 0 ) {
                ; /* Leave as INADDR_ANY */
            }
            else if ( isdigit(*host_part) ) {
                /*
      * Numeric IP address:
      */
                ap->sin_addr.s_addr =
                        inet_addr(host_part);
                // if ( ap->sin_addr.s_addr == INADDR_NONE ) {
                if ( !inet_aton(host_part,&ap->sin_addr) ) {
                    return -1;
                }
            }
            else {
                /*
    * Assume a hostname:
    */
                hp = gethostbyname(host_part);
                if ( !hp ) {
                    return -1;
                }
                if ( hp->h_addrtype != AF_INET ) {
                    return -1;
                }
                ap->sin_addr = * (struct in_addr *)
                        hp->h_addr_list[0];
            }

            /*
    * Process an optional port #:
    */
            if ( !strcmp(port_part, "*" ) ) {
                /* Leave as wild (zero) */
            }
            else if ( isdigit(*port_part) ) {
                /*
    * Process numeric port #:
    */
                lv = strtol(port_part,&cp,10);
                if ( cp != NULL && *cp ) {
                    return -2;
                }
                if ( lv < 0L || lv >= 32768 ) {
                    return -2;
                }
                ap->sin_port = htons( (short)lv);
            }
            else {
                /*
    * Lookup the service:
    */
                sp = getservbyname( port_part, protocol);
                if ( !sp ) {
                    return -2;
                }
                ap->sin_port = (short) sp->s_port;
            }

            /*
    * Return address length
    */
            *addrlen = sizeof *ap;

            free(inp_addr);
            return 0;
}


bool UDPSocket::displayError(const char *on_what) const
{
    fputs(strerror(errno),stdout);
    fputs(": ",stdout);
    fputs(on_what,stdout);
    fputc('\n',stdout);
    return false;
}


/*!
 * \brief create client
 * \param address_port the format are "IP:Port"
 */
UDPClient::UDPClient(const std::string &address_port)
{
    if ( ! UDPSocket::create(address_port.c_str(), "udp") )
        throw UDPSocketException("Could not create client socket.");
    if ( ! UDPSocket::option(SO_REUSEADDR) )
        throw UDPSocketException("Could not set socket to reuse address.");
    if ( ! UDPSocket::binding() )
        throw UDPSocketException("Could not bind to port.");
}

/*!
 * \brief create client
 * \param address IP number
 * \param port Port number
 */
UDPClient::UDPClient(const std::string &address, int port)
{
    static std::stringstream address_port;
    address_port << address << ':' << port;

    if ( ! UDPSocket::create(address_port.str().c_str(), "udp") )
        throw UDPSocketException("Could not create client socket.");
    if ( ! UDPSocket::option(SO_REUSEADDR) )
        throw UDPSocketException("Could not set socket to reuse address.");
    if ( ! UDPSocket::binding() )
        throw UDPSocketException("Could not bind to port.");
}

/*!
 * \brief send data
 * \param s
 */
const UDPClient &UDPClient::operator <<(const std::string & s) const
{
    if ( ! UDPSocket::send ( s ) )
        throw UDPSocketException("Could not write to socket.");
    return *this;
}

/*!
 * \brief send data
 * \param i
 */
const UDPClient &UDPClient::operator <<(const int & i) const
{
    static std::stringstream ss;
    ss << i;

    if ( ! UDPSocket::send ( ss.str() ) )
        throw UDPSocketException("Could not write to socket.");
    return *this;
}

/*!
 * \brief recieve data
 * \param s
 */
const UDPClient &UDPClient::operator >>(std::string & s)
{
    if ( ! UDPSocket::recv ( s ) )
        throw UDPSocketException("Could not read from socket.");
   return *this;
}


/*!
 * \brief create broadcastserver
 * \param address_port the format are "IP:Port"
 * \param interface broadcast in which interface (eth0, wlan0, or ppp0)
 */
UDPServer::UDPServer(const std::string &address_port, const std::string &interface)
{
    if ( ! UDPSocket::create(address_port.c_str(), "udp") )
        throw UDPSocketException("Could not create server socket.");
    if ( ! UDPSocket::option(SO_BROADCAST) )
        throw UDPSocketException("Could not set socket to broadcast.");
    if ( ! UDPSocket::binding(interface.c_str()) )
        throw UDPSocketException("Could not bind to port.");
}

/*!
 * \brief create server
 * \param address IP number
 * \param port Port number
 * \param interface broadcast in which interface (eth0, wlan0, or ppp0)
 */
UDPServer::UDPServer(const std::string &address, int port, const std::string &interface)
{
    static std::stringstream address_port;
    address_port << address << ':' << port;

    if ( ! UDPSocket::create(address_port.str().c_str(), "udp") )
        throw UDPSocketException("Could not create server socket.");
    if ( ! UDPSocket::option(SO_BROADCAST) )
        throw UDPSocketException("Could not set socket to broadcast.");
    if ( ! UDPSocket::binding(interface.c_str()) )
        throw UDPSocketException("Could not bind to port.");
}

/*!
 * \brief send/broadcast data
 * \param s
 */
const UDPServer &UDPServer::operator <<(const std::string & s) const
{
    if ( ! UDPSocket::send ( s.c_str(), s.length() ) )
        throw UDPSocketException("Could not write to socket.");
    return *this;
}

/*!
 * \brief send/broadcast data
 * \param i
 */
const UDPServer &UDPServer::operator <<(const int & i) const
{
    static std::stringstream ss;
    ss << i;

    if ( ! UDPSocket::send ( ss.str().c_str(), ss.str().length() ) )
        throw UDPSocketException("Could not write to socket.");
    return *this;
}
