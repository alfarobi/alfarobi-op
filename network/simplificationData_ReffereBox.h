#ifndef SIMPLIFICATIONDATA_REFFEREBOX_H
#define SIMPLIFICATIONDATA_REFFEREBOX_H

#include "data/RoboCupGameControll.h"
#include "data/SimplificatonReffereBox.h"

#define NO_TEAM_ALFAROBI    7
#define NO_PLAYER_ALFAROBI  1

namespace afr {

//extern uint8 getMyTeamColorID(RoboCupGameControlData &juri);


//extern int getGoalChange(RoboCupGameControlData &juri);

/*!
 * \brief get condition of match
 * \return negative mean chance to lose. positive mean chance to win
 */
extern int getMatchCondition();

/*!
 * simplify the status of RoboCupGameControlData
 * \param juri
 * \return data. -1 mean there are some error
 * \sa data/SimplificatonReffereBox.h
 */
extern int getStatusReffere(RoboCupGameControlData &juri);


/*!
 * get target goal
 * \param juri
 * \return 'b' mean GOAL_BLUE. 'y' mean GOAL_YELLOW
 */
extern char getTargetGoal(RoboCupGameControlData &juri);

}

#endif // SIMPLIFICATIONDATA_REFFEREBOX_H
