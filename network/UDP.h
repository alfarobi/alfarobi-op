/*
 *   UDP.h
 *
 *   Author: Alfarobi
 *
 */

#ifndef UDPSocket_H
#define UDPSocket_H

#define Struct2String(x) ((char*)&x)

#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string>

namespace afr
{

class UDPSocket
{
public:
    UDPSocket();
    UDPSocket(const char *address_port, char *protocol);
    virtual ~UDPSocket();

    bool create(const char *address_port, const char *protocol);
    bool option(const int &opt);
    bool binding();
    bool binding(const char *interface);

    bool recv(void *data, uint length);
    bool recv(std::string&);
    bool send(const void *data, uint length) const;
    bool send(const std::string) const;

private:
    uint x;
    struct sockaddr_in adr;  /* AF_INET */
    int len_inet;            /* length */
    int s;                   /* Socket */
    static int so_optaddr;

    bool displayError(const char *on_what) const;
    int mkaddr(void *addr,
               int *addrlen,
               const char *str_addr,
               const char *protocol);
};

class UDPSocketException
{
private:
    std::string m_s;

public:
    UDPSocketException ( std::string s ) : m_s ( s ) {}
    ~UDPSocketException (){}

    std::string description() { return m_s; }
};

class UDPClient : public UDPSocket
{
public:
    UDPClient(const std::string &address_port);
    UDPClient(const std::string &address, int port);
    UDPClient() {}
    virtual ~UDPClient() {}

    const UDPClient& operator << ( const std::string& ) const;
    const UDPClient& operator << ( const int& ) const;
    const UDPClient& operator >> ( std::string& );
};

class UDPServer : public UDPSocket
{
public:
    UDPServer(const std::string &address_port, const std::string &interface);
    UDPServer(const std::string &address, int port, const std::string &interface);
    UDPServer() {}
    virtual ~UDPServer() {}

    const UDPServer& operator << ( const std::string& ) const;
    const UDPServer& operator << ( const int& ) const;
};

}

#endif // UDPSocket_H
