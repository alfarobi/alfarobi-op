/*
 *   TCP.h
 *
 *   Author: Alfarobi
 *
 */

#ifndef _TCP_NETWORK_H_
#define _TCP_NETWORK_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <arpa/inet.h>

#define Struct2String(x) ((unsigned char*)&x)

namespace afr
{

class TCPSocket
{
private:
    int m_sock;
    sockaddr_in m_addr;

public:
    static const int MAXHOSTNAME = 200;
    static const int MAXCONNECTIONS = 5;
    static const int MAXRECV = 500;

    TCPSocket();
    virtual ~TCPSocket();

    // Server initialization
    bool create();
    bool bind ( const int port );
    bool listen() const;
    bool accept ( TCPSocket& ) const;

    // Client initialization
    bool connect ( const std::string host, const int port );

    // Data Transimission
    bool send ( const std::string ) const;
    bool send ( void* data, int length ) const;
    int recv ( std::string& ) const;
    int recv ( void* data, int length ) const;

    void set_non_blocking ( const bool );

    bool is_valid() const { return m_sock != -1; }
};

class TCPSocketException
{
private:
    std::string m_s;

public:
    TCPSocketException ( std::string s ) : m_s ( s ) {}
    ~TCPSocketException (){}

    std::string description() { return m_s; }
};

class TCPServer : public TCPSocket
{
public:
    TCPServer ( int port );
    TCPServer (){}
    virtual ~TCPServer();

    const TCPServer& operator << ( const std::string& ) const;
    const TCPServer& operator << ( const int& ) const;
    const TCPServer& operator >> ( std::string& ) const;

    void accept ( TCPServer& );
};


class TCPClient : public TCPSocket
{
public:
    TCPClient ( int port );
    TCPClient (){}
    virtual ~TCPClient();

    const TCPClient& operator << ( const std::string& ) const;
    const TCPClient& operator << ( const int& ) const;
    const TCPClient& operator >> ( std::string& ) const;

    void connect (const std::string host, int port );
};

}

#endif
