/*
 *   TCPNetwork.cpp
 *
 *   Author: ROBOTIS
 *
 */
#include <iostream>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sstream>
#include "TCP.h"

using namespace afr;
using namespace std;

/*!
 * \brief TCPSocket::TCPSocket
 */
TCPSocket::TCPSocket() : m_sock ( -1 )
{
    memset ( &m_addr, 0, sizeof ( m_addr ) );
}

/*!
 * \brief TCPSocket::~TCPSocket
 */
TCPSocket::~TCPSocket()
{
    if ( is_valid() )
        ::close ( m_sock );
}

/*!
 * \brief TCPSocket::create
 * \return
 */
bool TCPSocket::create()
{
    m_sock = socket ( AF_INET,
                      SOCK_STREAM,
                      0 );

    if ( ! is_valid() )
        return false;

    // TIME_WAIT - argh
    int on = 1;
    if ( setsockopt ( m_sock, SOL_SOCKET, SO_REUSEADDR, ( const char* ) &on, sizeof ( on ) ) == -1 )
        return false;

    return true;
}

/*!
 * \brief TCPSocket::bind
 * \param port
 * \return
 */
bool TCPSocket::bind ( const int port )  {
    if ( ! is_valid() )
    {
        return false;
    }

    m_addr.sin_family = AF_INET;
    m_addr.sin_addr.s_addr = INADDR_ANY;
    m_addr.sin_port = htons ( port );

    int bind_return = ::bind ( m_sock,
                             ( struct sockaddr * ) &m_addr,
                             sizeof ( m_addr ) );
    if ( bind_return == -1 )
    {
        return false;
    }

    return true;
}

/*!
 * \brief TCPSocket::listen
 * \return
 */
bool TCPSocket::listen() const
{
    if ( ! is_valid() )
    {
        return false;
    }

    int listen_return = ::listen ( m_sock, MAXCONNECTIONS );

    if ( listen_return == -1 )
    {
        return false;
    }
    return true;
}

/*!
 * \brief TCPSocket::accept
 * \param new_socket
 * \return
 */
bool TCPSocket::accept ( TCPSocket& new_socket ) const
{
    int addr_length = sizeof ( m_addr );
    new_socket.m_sock = ::accept ( m_sock, ( sockaddr * ) &m_addr, ( socklen_t * ) &addr_length );

    if ( new_socket.m_sock <= 0 )
        return false;
    else
        return true;
}

/*!
 * \brief TCPSocket::send
 * \param s
 * \return
 */
bool TCPSocket::send ( const std::string s ) const
{
    int status = ::send ( m_sock, s.c_str(), s.size(), MSG_NOSIGNAL );
    if ( status == -1 )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*!
 * \brief TCPSocket::send
 * \param data
 * \param length
 * \return
 */
bool TCPSocket::send ( void* data, int length ) const
{
    int status = ::send ( m_sock, data, length, MSG_NOSIGNAL );
    if ( status == -1 )
    {
        return false;
    }
    else
    {
        return true;
    }
}

/*!
 * \brief TCPSocket::recv
 * \param s
 * \return
 */
int TCPSocket::recv ( std::string& s ) const
{
    char buf [ MAXRECV + 1 ];

    s = "";

    memset ( buf, 0, MAXRECV + 1 );

    int status = ::recv ( m_sock, buf, MAXRECV, 0 );

    if ( status == -1 )
    {
        cout << "status == -1   errno == " << errno << "  in Socket::recv\n";
        return 0;
    }
    else if ( status == 0 )
    {
        return 0;
    }
    else
    {
        s = buf;
        return status;
    }
}

/*!
 * \brief TCPSocket::recv
 * \param data
 * \param length
 * \return
 */
int TCPSocket::recv ( void* data, int length ) const
{
	int status = ::recv ( m_sock, data, length, 0 );

    if ( status == -1 )
    {
        cout << "status == -1   errno == " << errno << "  in Socket::recv\n";
        return 0;
    }
    else if ( status == 0 )
    {
        return 0;
    }
    
    return status;
}

/*!
 * \brief TCPSocket::connect
 * \param host
 * \param port
 * \return
 */
bool TCPSocket::connect ( const std::string host, const int port )
{
    if ( ! is_valid() ) return false;

    m_addr.sin_family = AF_INET;
    m_addr.sin_port = htons ( port );

    int status = inet_pton ( AF_INET, host.c_str(), &m_addr.sin_addr );

    if ( errno == EAFNOSUPPORT ) return false;

    status = ::connect ( m_sock, ( sockaddr * ) &m_addr, sizeof ( m_addr ) );

    if ( status == 0 )
        return true;
    else
        return false;
}

/*!
 * \brief TCPSocket::set_non_blocking
 * \param b
 */
void TCPSocket::set_non_blocking ( const bool b )
{
    int opts;

    opts = fcntl ( m_sock, F_GETFL );

    if ( opts < 0 )
    {
        return;
    }

    if ( b )
        opts = ( opts | O_NONBLOCK );
    else
        opts = ( opts & ~O_NONBLOCK );

    fcntl ( m_sock,
            F_SETFL,opts );
}


/*!
 * \brief TCPServer::TCPServer
 * \param port
 */
TCPServer::TCPServer ( int port )
{
    if ( ! TCPSocket::create() )
    {
        throw TCPSocketException ( "Could not create server socket." );
    }

    if ( ! TCPSocket::bind ( port ) )
    {
        throw TCPSocketException ( "Could not bind to port." );
    }

    if ( ! TCPSocket::listen() )
    {
        throw TCPSocketException ( "Could not listen to socket." );
    }
}

/*!
 * \brief TCPServer::~TCPServer
 */
TCPServer::~TCPServer()
{
}

/*!
 * \brief TCPServer::operator <<
 * \param s
 * \return
 */
const TCPServer& TCPServer::operator << ( const std::string& s ) const
{	
    if ( ! TCPSocket::send ( s ) )
    {
        throw TCPSocketException ( "Could not write to socket." );
    }

    return *this;
}

/*!
 * \brief TCPServer::operator <<
 * \param i
 * \return
 */
const TCPServer& TCPServer::operator << ( const int& i ) const
{
    std::stringstream ss;
    ss << i;

    if ( ! TCPSocket::send ( ss.str() ) )
    {
        throw TCPSocketException ( "Could not write to socket." );
    }

    return *this;
}

/*!
 * \brief TCPServer::operator >>
 * \param s
 * \return
 */
const TCPServer& TCPServer::operator >> ( std::string& s ) const
{
    if ( ! TCPSocket::recv ( s ) )
    {
        throw TCPSocketException ( "Could not read from socket." );
    }

    return *this;
}

/*!
 * \brief TCPServer::accept
 * \param sock
 */
void TCPServer::accept ( TCPServer& sock )
{
    if ( ! TCPSocket::accept ( sock ) )
    {
        throw TCPSocketException ( "Could not accept socket." );
    }
}

/*!
 * \brief TCPClient::TCPClient
 * \param port
 */
TCPClient::TCPClient(int port)
{
    if ( ! TCPSocket::create() )
    {
        throw TCPSocketException ( "Could not create client socket." );
    }

    if ( ! TCPSocket::bind ( port ) )
    {
        throw TCPSocketException ( "Could not bind to port." );
    }
}

/*!
 * \brief TCPClient::~TCPClient
 */
TCPClient::~TCPClient()
{
}

/*!
 * \brief TCPClient::operator <<
 * \param s
 * \return
 */
const TCPClient& TCPClient::operator << ( const std::string& s ) const
{
    if ( ! TCPSocket::send ( s ) )
    {
        throw TCPSocketException ( "Could not write to socket." );
    }

    return *this;
}

/*!
 * \brief TCPClient::operator <<
 * \param i
 * \return
 */
const TCPClient& TCPClient::operator << ( const int& i ) const
{
    std::stringstream ss;
    ss << i;

    if ( ! TCPSocket::send ( ss.str() ) )
    {
        throw TCPSocketException ( "Could not write to socket." );
    }

    return *this;
}

/*!
 * \brief TCPClient::operator >>
 * \param s
 * \return
 */
const TCPClient& TCPClient::operator >> ( std::string& s ) const
{
    if ( ! TCPSocket::recv ( s ) )
    {
        throw TCPSocketException ( "Could not read from socket." );
    }

    return *this;
}

/*!
 * \brief TCPClient::connect
 * \param host
 * \param port
 */
void TCPClient::connect( const std::string host, int port )
{
    if ( ! TCPSocket::connect(host, port) )
    {
        throw TCPSocketException ( "Could not connect server socket." );
    }
}
