#include "simplificationData_ReffereBox.h"

using namespace afr;

#define juri_myteam			juri.teams[warna_team_alfarobi]
#define juri_sebagai(self)  juri.teams[warna_team_alfarobi].players[self]
#define juri_myself         juri_sebagai(NO_PLAYER_ALFAROBI-1)

#define juri_enemyteam      juri.teams[warna_team_musuh]

static bool cariWarnaTeam_aktive = false;
static bool goalChange_flag = false;
static uint8 warna_team_alfarobi;
static uint8 warna_team_musuh;

static int score_vector = 0;

uint8 getMyTeamColorID(RoboCupGameControlData &juri)
{
    for (int var = 0; var < 2; ++var) {
        if (juri.teams[var].teamNumber == NO_TEAM_ALFAROBI)
            return juri.teams[var].teamColour;
    }
    return 'f';
}


int getGoalChange(RoboCupGameControlData &juri)
{
    uint8 last_myScore, last_enemyScore;
    int cond, last_score_vector;

    if(!cariWarnaTeam_aktive) {
        warna_team_alfarobi = getMyTeamColorID(juri);
        if (warna_team_alfarobi == 'f') return -1;
        cariWarnaTeam_aktive = true;
    }

    if(!goalChange_flag) {
        last_myScore = juri_myteam.score;
        last_enemyScore = juri_enemyteam.score;
        last_score_vector = 0;
        goalChange_flag = true;
    }

    if(juri_myteam.score > last_myScore) score_vector++;
    if (juri_enemyteam.score < last_enemyScore) score_vector--;

    if(score_vector > last_score_vector)  cond = REF_STATE_MY_GOAL;
    else if(score_vector < last_score_vector) cond = REF_STATE_ENEMY_GOAL;
    else cond = -1; // not changed

    last_myScore = juri_myteam.score;
    last_enemyScore = juri_enemyteam.score;
    last_score_vector = score_vector;

    return cond;
}


int afr::getStatusReffere(RoboCupGameControlData &juri)
{
    static int goale;

    if(!cariWarnaTeam_aktive)
    {
        warna_team_alfarobi = getMyTeamColorID(juri);
        if (warna_team_alfarobi == 'f') return -1;
        if (warna_team_alfarobi > 0) warna_team_musuh = 0;
        else warna_team_musuh = 1;
        cariWarnaTeam_aktive = true;
    }

    // NOTE ; jangan diubah urutannya karena pke if2 an. kalo sesuai kondisi maka ga dicek kondisi lainnya
    if(juri.version == GAMECONTROLLER_STRUCT_VERSION)
    {
        //---------for detecting weight of match-----------------------
        goale = getGoalChange(juri);
        if(goale != -1) return goale;
        //-------------------------------------------------------------

        if(juri_myself.penalty == PENALTY_HL_KID_REQUEST_FOR_PICKUP)
            return REF_STATE_PICKUP;
        else if(juri_myself.penalty == PENALTY_HL_KID_REQUEST_FOR_SERVICE)
            return REF_STATE_SERVICE;
        else if(juri.dropInTeam == warna_team_alfarobi && juri.dropInTime < 10 && juri.dropInTime > 0)
            return REF_STATE_DROPIN; // 10 detik setelah OUT
        else if(juri_myself.penalty != PENALTY_NONE)
            return REF_STATE_FOUL;

        else if(juri.state == STATE_INITIAL)
            return REF_STATE_INIT;
        else if(juri.state == STATE_READY)
            return REF_STATE_READY;
        else if(juri.state == STATE_SET)
            return REF_STATE_SET;
        else if(juri.kickOffTeam == warna_team_alfarobi && juri.state == STATE_PLAYING && (juri.secondaryState == STATE2_NORMAL || juri.secondaryState == STATE2_OVERTIME))
            return REF_STATE_MY_KICKOFF; // alfarobi kickoff
        else if(juri.kickOffTeam != warna_team_alfarobi && juri.state == STATE_PLAYING && (juri.secondaryState == STATE2_NORMAL|| juri.secondaryState == STATE2_OVERTIME))
            return REF_STATE_ENEMY_KICKOFF; // musuh kickoff
        else if(juri.state == STATE_FINISHED)
            return REF_STATE_FINISH;

        else if(juri.kickOffTeam == warna_team_alfarobi && juri.secondaryState == STATE2_PENALTYSHOOT)
            return REF_STATE_MY_PENALTYSHOOT;
        else if(juri.kickOffTeam != warna_team_alfarobi && juri.secondaryState == STATE2_PENALTYSHOOT)
            return REF_STATE_ENEMY_PENALTYSHOOT;

        else if(juri.kickOffTeam == DROPBALL && juri.state == STATE_PLAYING && (juri.secondaryState == STATE2_NORMAL || juri.secondaryState == STATE2_OVERTIME))
            return REF_STATE_DROPBALL; // kickoff siapa cepat dia dapat (langsung tendang gawang coy)
        else
            return -1;
    }
    return -1;
}


char afr::getTargetGoal(RoboCupGameControlData &juri)
{
    if(!cariWarnaTeam_aktive)
    {
        warna_team_alfarobi = getMyTeamColorID(juri);
        if (warna_team_alfarobi == 'f') return -1;
        cariWarnaTeam_aktive = true;
    }

    if (juri.teams[warna_team_alfarobi].goalColour == GOAL_BLUE)
        return 'b';
    else if(juri.teams[warna_team_alfarobi].goalColour == GOAL_YELLOW)
        return 'y';
//    else
    return 'f';
}


int afr::getMatchCondition()
{
    return score_vector;
}
