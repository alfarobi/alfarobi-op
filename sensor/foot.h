#ifndef FOOT_H
#define FOOT_H

#include <stdint-gcc.h>
#include "../std/logfile.h"

namespace afr {

/*!
 * \brief acces the FSR
 */
class ForceSensor
{
private:
    struct Pos { uint32_t x,y; };

public:
    ForceSensor();

    void open();
    void setThreshold(uint32_t &x, uint32_t &y, uint32_t &minmax);

    Pos read();
    bool isPressed();

    void logDataEnable(const std::string &filename, const double &each_seconds);

private:
    struct MinMax { uint32_t min,max; };
    struct Thresh { MinMax x,y; } thresh;
    Pos pos;

    LogFile *log;
    double time_gap;

    void logData();
};


/*!
 * \brief get the data of FSR in Foot
 * \sa ForceSensor
 */
class Foot : public ForceSensor
{
public:
    static Foot* GetInstance() { return m_UniqueInstance; } /*!< this class is link list. */
    Foot();

    void open(ForceSensor &left, ForceSensor &right);

    bool isLeftinGround();
    bool isRightinGround();
    bool is2FootinGround();

    uint32_t countLeftStep();
    uint32_t countRightStep();
    uint32_t countAllStep();
    uint32_t countJumpStep();

private:
    static Foot *m_UniqueInstance;

    ForceSensor *left;
    ForceSensor *right;
};

}

#endif // FOOT_H
