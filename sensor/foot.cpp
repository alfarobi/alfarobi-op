#include "foot.h"

using namespace afr;

Foot* Foot::m_UniqueInstance = new Foot();
static bool log_once = true;

ForceSensor::ForceSensor()
{
    pos.x = 0;
    pos.y = 0;
}

/*!
 * \brief open device
 */
void ForceSensor::open()
{
}

/*!
 * \brief set threshold
 * \param x
 * \param y
 * \param minmax
 * \sa ForceSensor::isPressed()
 */
void ForceSensor::setThreshold(uint32_t &x, uint32_t &y, uint32_t &minmax)
{
    thresh.x.min = x-minmax;
    thresh.x.max = x+minmax;
    thresh.y.min = y-minmax;
    thresh.y.max = y+minmax;
}

/*!
 * \brief read the force of FSR
 * \code
 *      ForceSensor fsr;
 *      cout << fsr.read().x << fsr.read().y;
 * \endcode
 * \return position that consist 32 bit integer x,y
 */
ForceSensor::Pos ForceSensor::read()
{
    // TODO : read data sensor
}

/*!
 * \brief get the status if the data between threshold
 * \return true if the reading is between threshold
 * \sa ForceSensor::setThreshold()
 */
bool ForceSensor::isPressed()
{
    pos = read();
    logData();
    return ( (thresh.x.min <= pos.x && pos.x <= thresh.x.max) && (thresh.y.min <= pos.y && pos.y <= thresh.y.max) );
}

/*!
 * \brief Enable data logging and save it under *.log extension.
 *
 *      The data that logged is form ForceSensor::isPressed() function.
 * It consist : x, y. The separator of the data is <TAB> white space.
 * You can plot the log data in spradsheet by importing it.
 *
 * \param filename  must be *.ini extension
 * \param each_seconds  log data for each second
 * \sa ForceSensor::isPressed()
 */
void ForceSensor::logDataEnable(const std::string &filename, const double &each_seconds)
{
    log = new LogFile(filename);
    time_gap = each_seconds;
}

/*!
 * \brief write log file
 */
void ForceSensor::logData()
{
    if (time_gap > 0) {
        static double dt;

        if (log_once) {
            log->logData("time");
            log->logData("x");
            log->logData("y");
            log_once = false;
        }

        if ((dt += log->getdiffTime()) >= time_gap) {
            log->createLine(); // it will print the time
            log->logData(pos.x);
            log->logData(pos.y);
            dt = 0;
        }
    }
}



/*!
 * \brief set wich force sensor is being right or left foot
 * \param left
 * \param right
 */
Foot::Foot()
{
}

void Foot::open(ForceSensor &left, ForceSensor &right)
{
    this->left = &left;
    this->right = &right;
}

bool Foot::isLeftinGround()
{
    return left->isPressed();
}

bool Foot::isRightinGround()
{
    return right->isPressed();
}

bool Foot::is2FootinGround()
{
    return ( isLeftinGround() && isRightinGround() );
}

uint32_t Foot::countLeftStep()
{
    static bool temp = false;
    static uint32_t count;

    if ( (!isLeftinGround()) && temp) count++;
    temp = isLeftinGround();
    return count;
}

uint32_t Foot::countRightStep()
{
    static bool temp = false;
    static uint32_t count;

    if ( (!isRightinGround()) && temp) count++;
    temp = isRightinGround();
    return count;
}

uint32_t Foot::countAllStep()
{
    return countLeftStep() + countRightStep();
}

uint32_t Foot::countJumpStep()
{
    static bool temp = false;
    static uint32_t count;

    if ( (!is2FootinGround()) && temp) count++;
    temp = is2FootinGround();
    return count;
}
