/* Sensor Compass CMPS10
 *
 * Author : Alfarobi
 *
 * Status : NOT Tested
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "cmps10.h"
#include <sstream>
#include <opencv2/core/core.hpp>

using namespace LibSerial;
using namespace afr;
using namespace std;


CMPS10::CMPS10()
{
    serial_enable = false;
}

/*!
 * \brief open device path (for SerialPort use)
 * \param dev path interface.Example "/dev/ttyUSB0"
 * \return 'true' if connected else 'false'
 */
bool CMPS10::open(const std::string &dev)
{
    serial = new SerialStream();

    serial->Open(dev);
    if (serial->IsOpen())
    {
        serial->SetBaudRate(SerialStreamBuf::BAUD_9600);
        serial->SetCharSize(SerialStreamBuf::CHAR_SIZE_8);
        serial->SetFlowControl(SerialStreamBuf::FLOW_CONTROL_NONE);
        serial->SetNumOfStopBits(2);
        serial->SetParity(SerialStreamBuf::PARITY_NONE);
    } else
    {
        fprintf(stderr, "can't open serial\n");
        serial_enable = false;
        return false;
    }
    serial_enable = true;
    return true;
}

/*!
 * \brief open device serial
 * \param serial
 */
void CMPS10::open(SerialStream &serial)
{
    this->serial = &serial;
    serial_enable = true;
}

/*!
 * \brief connect to CMPS10 in the uGM_Cxx controller
 * \param controller
 */
void CMPS10::open(uGM_Cxx &controller)
{
    this->controller = &controller;
    serial_enable = false;
}

/*!
 * \brief Search/try open along n with exception t. Example search "/dev/ttyUSB0" to "/dev/ttyUSB5" except "/dev/ttyUSB4"
 * \param n search within n device
 * \param except don't search or try n device. if -1 don't except anything
 * \return id of the correct device. If not found return -1
 */
int CMPS10::autoSearch(uint32_t n, int except)
{
    static stringstream ss;
    ss << "/dev/ttyUSB";
    for (uint32_t i = 0; i < n; ++i) {
        if ((int)n != except) {
            ss << i;
            if (open(ss.str()) && check_version()) {
                serial_enable = true;
                return i;
            }
            ss.str(string()); // clear the stream
            serial->Close();
        }
    }
    return -1;
}

/*!
 * \brief get version of CMPS10
 * \return version of CMPS10
 */
int CMPS10::getVersion()
{
    if (serial_enable) {
        write = 0x11;
        return get(write);
    } else {
        return controller->readByte(uGM_Cxx::CMPS10_VERSION);
    }
}

/*!
 * \brief get heading of compass with precission 1.4 degree
 * \return heading of compass
 */
uint16_t CMPS10::getBearing()
{
    if (serial_enable) {
        write = 0x12;
        return get(write)*360/255;
    } else
        return controller->readByte(uGM_Cxx::CMPS10_HEAD_BYTE)*360/255;
}

/*!
 * \brief get heading of compass with precission 0.1 degree
 * \return heading or X angle of compass
 */
double CMPS10::getBearingD()
{
    if (serial_enable) {
        static uint16_t data;
        write = 0x13;
        serial->write(&write, 1);
        serial->read((char*)&data, 2);
        return data/10;
    } else
        return controller->readWord(uGM_Cxx::CMPS10_HEAD_H)/10;
}

/*!
 * \brief get pitch of compass with precission 1.4 degree
 * \return pitch or Y angle of compass +/- 85 degree
 */
uint16_t CMPS10::getPitch()
{
    if (serial_enable) {
        write = 0x14;
        return get(write)*360/255;
    } else
        return controller->readByte(uGM_Cxx::CMPS10_PITCH_BYTE)*360/255;
}

/*!
 * \brief get roll of compass with precission 1.4 degree
 * \return roll or Z angle of compass +/- 85 degree
 */
uint16_t CMPS10::getRoll()
{
    if (serial_enable) {
        write = 0x15;
        return get(write)*360/255;
    } else
        return controller->readByte(uGM_Cxx::CMPS10_ROLL_BYTE)*360/255;
}

/*!
 * \brief get raw data of magnetometer
 * \return 6 Byte, each axis consist 2 Byte
 */
CMPS10::Raw CMPS10::getMagneto()
{
    static Raw mag;

    if (serial_enable) {
        write = 0x21;
        serial->write(&write, 1);
        serial->read((char*)&mag, 6);
    } else {
        temp.set = controller->readWord(uGM_Cxx::CMPS10_MAGNETO_X_H);
        mag.x = temp.var;
        temp.set = controller->readWord(uGM_Cxx::CMPS10_MAGNETO_Y_H);
        mag.y = temp.var;
        temp.set = controller->readWord(uGM_Cxx::CMPS10_MAGNETO_Z_H);
        mag.z = temp.var;
    }

    return mag;
}

/*!
 * \brief get raw data of accelerometer
 * \return 6 Byte, each axis consist 2 Byte
 */
CMPS10::Raw CMPS10::getAccelero()
{
    static Raw acc;

    if (serial_enable) {
        write = 0x22;
        serial->write(&write, 1);
        serial->read((char*)&acc, 6);
    } else {
        temp.set = controller->readWord(uGM_Cxx::CMPS10_ACC_X_H);
        acc.x = temp.var;
        temp.set = controller->readWord(uGM_Cxx::CMPS10_ACC_Y_H);
        acc.y = temp.var;
        temp.set = controller->readWord(uGM_Cxx::CMPS10_ACC_Z_H);
        acc.z = temp.var;
    }

    return acc;
}


uint8_t CMPS10::get(char &write)
{
    static uint8_t buf;
    serial->write(&write, 1);
    serial->read((char*)&buf, 1);
    return buf;
}

bool CMPS10::check_version()
{
    return (getVersion() == CMPS10_VER);
}
