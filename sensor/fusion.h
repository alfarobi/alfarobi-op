/* Sensor Fusion
 *
 *List of function for combining 2 or more sensor data into the new data
 *
 */

#include "../std/custom_math.h"
#include <cmath>
#include <opencv2/core/core.hpp>

const double r = 35.8;
const double r_square = r*r;

const cv::Point3d mag_ref;

/*!
 * \brief compensated compass error with another compass data
 * \param mag1
 * \param mag2
 * \return desired 3 DOF magnetometer
 */
inline cv::Point3d doubleCompass(const cv::Point3d &mag1, const cv::Point3d &mag2)
{
    static double diff_x, diff_y, diff_z;
    static double m, c, A, B, C;

    static cv::Vec2d x_ref, y_ref, z_ref; // after correction

    diff_x = mag2.x - mag1.x;
    diff_y = mag2.y - mag1.y;
    diff_z = mag2.z - mag1.z;

    // -----------calculate x----------
    m =  diff_y / diff_x;
    c = m*(-mag1.x) + mag1.y;
    A = m*m + 1;
    B = 2*m*c;
    C = c*c - r_square;

    if (! quadraticSolver(A, B, C, &x_ref)) return cv::Point3d(NULL);
    //----------------------------------

    // -----------calculate y----------
    y_ref[0] = m*x_ref[0] + c;
    y_ref[1] = m*x_ref[1] + c;
    //----------------------------------

    // -----------calculate z----------
    m = diff_z / diff_y;
    c = m*(-mag1.y) + mag1.z;

    z_ref[0] = m*y_ref[0] + c;
    z_ref[1] = m*y_ref[1] + c;
    //----------------------------------

    return cv::Point3d(nearestPoint(mag2.x, mag1.x, x_ref),
                       nearestPoint(mag2.y, mag1.y, y_ref),
                       nearestPoint(mag2.z, mag1.z, z_ref));

    /*! Equation \abstract
     *
     *  circle reference : x^2 + y^2 = r^2
     *  line of 2 compass point : y - y1 = [(y2 - y1) / (x2 - x1)]*(x - x1)
     *                             <=> y = {[(y2-y1) / (x2-x1)]*x} - {[(y2-y1) / (x2-x1)]*x1 + y1}
     *  assume : y = mx + c
     *  divide :
     *      m = (y2 - y1) / (x2 - x1)
     *      c = m*(-x1) + y1
     *  then : x^2 + (mx+c)^2 == r^2
     *  another one :
     *      circle  : y = sqrt(r^2 - x^2)
     *      line    : y = mx + c
     *      then    :   (mx)^2 + c^2 + 2mxc = r^2 - x^2
     *             <=>  m^2*x^2 + 2mcx + x^2 = r^2 - c^2
     *             <=>  (m^2 + 1)*x^2 + 2mxc = (r^2 - c^2)
     *             <=>  (m^2 + 1) * x^2 + 2mc * x + (c^2 - r^2) = 0
     *      divide :
     *          A = (m^2 + 1)
     *          B = 2mc
     *          C = c^2 - r^2
     *  [MUMET : google "circle-line intersect equation"]
     *
     */
}

/*!
 * \brief
 * \param mag
 * \param acc
 * \return heading in radians
 */
inline float headingTiltCompensated(cv::Point3d mag, cv::Point3i acc)
{
  float rollRadians = asin(acc.y);
  float pitchRadians = asin(acc.x);

  // We cannot correct for tilt over 40 degrees with this algorthem, if the board is tilted as such, return 0.
  if(rollRadians > 0.78 || rollRadians < -0.78 || pitchRadians > 0.78 || pitchRadians < -0.78)
  {
    return 0;
  }

  // Some of these are used twice, so rather than computing them twice in the algorithem we precompute them before hand.
  float cosRoll = cos(rollRadians);
  float sinRoll = sin(rollRadians);
  float cosPitch = cos(pitchRadians);
  float sinPitch = sin(pitchRadians);

  // The tilt compensation algorithm.
  float Xh = mag.x*cosPitch + mag.z*sinPitch;
  float Yh = mag.x*sinRoll*sinPitch + mag.y*cosRoll - mag.z*sinRoll*cosPitch;

  float heading = atan2(Yh, Xh);

  return heading;
}
