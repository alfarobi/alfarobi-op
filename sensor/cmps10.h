/* Sensor Compass CMPS10
 *
 * Author : Alfarobi
 *
 * Status : NOT Tested
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef CMPS10_H
#define CMPS10_H

#define CMPS10_VER  7

#include <stdint-gcc.h>
#include <string>
#include <SerialStream.h>

#include "../motion/ugm_cxx.h"

namespace afr
{

/*!
 * \brief Class for accesing compass sensor CMPS10
 */
class CMPS10
{
private:
    struct Word {
        char high;
        char low;
    };
    struct Raw {
        Word x;
        Word y;
        Word z;
    };

public:
    CMPS10();

    bool open(const std::string &dev);
    void open(LibSerial::SerialStream &serial);
    void open(uGM_Cxx &controller);

    int autoSearch(uint32_t n, int except = -1);

    int getVersion();
    uint16_t getBearing();
    double getBearingD();
    uint16_t getPitch();
    uint16_t getRoll();

    Raw getMagneto();
    Raw getAccelero();

private:
    LibSerial::SerialStream *serial;
    uGM_Cxx *controller;

    bool serial_enable;
    char write;

    uint8_t get(char &write);
    bool check_version();

    union ConvertWord {
        Word var;
        uint16_t set;
    }temp;
};

}

#endif // CMPS10_H
