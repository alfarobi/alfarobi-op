TEMPLATE = subdirs
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXX += -std=gnu++11

LIBS += \
-lopencv_core -lopencv_highgui -lopencv_imgproc \
-lserial -lpthread -ltbb2

OTHER_FILES += \
    imgproc/README.md \
    motion/README.md \
    std/README.md \
    README.md \
    config.ini

SUBDIRS += \
    example

