#ifndef SIMPLIFICATONREFFEREBOX_H
#define SIMPLIFICATONREFFEREBOX_H

#define REF_STATE_INIT                  0
#define REF_STATE_READY                 1
#define REF_STATE_SET                   2

#define REF_STATE_DROPBALL              3 // kickoff siapa cepat dia dapat (langsung tendang gawang coy)
#define REF_STATE_MY_KICKOFF            4
#define REF_STATE_ENEMY_KICKOFF         5

#define REF_STATE_MY_PENALTYSHOOT       6
#define REF_STATE_ENEMY_PENALTYSHOOT    7

#define REF_STATE_FOUL                  8
#define REF_STATE_PICKUP                9
#define REF_STATE_SERVICE               10

#define REF_STATE_DROPIN                11  // kondisi 10 detik setelah OUT

#define REF_STATE_MY_GOAL               12
#define REF_STATE_ENEMY_GOAL            13

#define REF_STATE_FINISH                14

#endif // SIMPLIFICATONREFFEREBOX_H
