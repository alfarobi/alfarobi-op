TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lserial

INCLUDEPATH += ../../../motion

SOURCES += main.cpp \
    ../../../sensor/cmps10.cpp \
    ../../../motion/ugm_cxx.cpp

HEADERS += \
    ../../../sensor/fusion.h \
    ../../../sensor/cmps10.h \
    ../../../motion/ugm_cxx.h

