TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../../../ \
../../../motion/

LIBS += -lserial \
-lopencv_core -lopencv_highgui -lopencv_imgproc

SOURCES += main.cpp \
    ../../../motion/head.cpp \
    ../../../std/minIni/minIni.c \
    ../../../imgproc/momentdetection.cpp \
    ../../../motion/ugm_cxx.cpp

HEADERS += \
    ../../../motion/subcontroller.h \
    ../../../motion/head.h \
    ../../../motion/ugm_cxx.h

