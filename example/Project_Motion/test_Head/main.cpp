/* Head Configuration, Calibration, and Testing the function
 *
 * check each case to understand the example
 *
 * BUG : to complicated :D
 */

#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include "imgproc/momentdetection.h"

#include "head.h"

#define FILE_HEAD_CONFIG "head.ini"

using namespace std;
using namespace afr;

cv::VideoCapture cam;
cv::Mat img;
MomentDetection *ball;
minIni *ini;

#define kepala Head::GetInstance()

bool detected;

void highLevel(uint16_t search_degreeIncrement, useconds_t search_delayStep, uint8_t kalmanIteration);
void debug();

int main()
{
    int choice, subchoice;
    ini = new minIni(FILE_HEAD_CONFIG);

    cout << "========= Head Controll ========="
         << "1. Low-level \n"
         << "2. Medium level \n"
         << "3. High level (ball tracking & searching) \n";
    cin >> choice;

    switch (choice) {
    case 1: {
        double x_angle, y_angle;

        cout << "========= Low-level Controll ========="
             << "1. Pan (rotate along x) \n"
             << "2. Tilt (rotate along y) \n"
             << "3. Pan Tilt (moveAtAngle) \n"
             << "4. Cosinus Move \n";
        cin >> subchoice;

        switch (subchoice) {
        case 1:
            cout << "\nAngle : "; cin >> x_angle;
            kepala->panning(x_angle);
            break;
        case 2:
            cout << "\nAngle : "; cin >> y_angle;
            kepala->tilting(y_angle);
            break;
        case 3:
            cout << "\nTilt Angle : "; cin >> y_angle;
            cout << "\nPan Angle ; "; cin >> x_angle;
            kepala->moveAtAngle(y_angle, x_angle);
            break;
        case 4:
            int multiplier;
            cout << "\nDegree : "; cin >> x_angle;
            cout << "\nMultiplier : "; cin >> multiplier;
            kepala->cosinusMove(x_angle, multiplier);
            break;
        default:
            break;
        }
        break;
    }
    case 2: {
        double A_angle_offset, B_angle_increment;
        useconds_t delay_step; // in microseconds = 1/100000 s

        cout << "========= Medium-level Controll ========="
             << "1. Nod \n"
             << "2. Sweep left \n"
             << "3. Sweep right \n";
        cin >> subchoice;

        switch (subchoice) {
        case 1:
            cout << "\nTilt angle offset : "; cin >> A_angle_offset;
            cout << "\nPan angle increment step : "; cin >> B_angle_increment;
            cout << "\nDelay between angle increment (us) : "; cin >> delay_step;
            kepala->nod(A_angle_offset, B_angle_increment, delay_step);
            break;
        case 2:
            cout << "\nPan angle offset : "; cin >> A_angle_offset;
            cout << "\nTilt angle increment step : "; cin >> B_angle_increment;
            cout << "\nDelay between angle increment (us) : "; cin >> delay_step;
            kepala->sweepLeft(A_angle_offset, B_angle_increment, delay_step);
            break;
        case 3:
            cout << "\nPan angle offset : "; cin >> A_angle_offset;
            cout << "\nTilt angle increment step : "; cin >> B_angle_increment;
            cout << "\nDelay between angle increment (us) : "; cin >> delay_step;
            kepala->sweepRight(A_angle_offset, B_angle_increment, delay_step);
            break;
        default:
            break;
        }
        break;
    }
    case 3: {
        uint16_t degreeIncrement; useconds_t delayStep; uint8_t kalmanIteration = 0;
        char yes_no;
        cout << "\nDegree increment for searching : "; cin >> degreeIncrement;
        cout << "\nDelay Step for searching : "; cin >> delayStep;

        do {
            cout << "\n Load head.ini configuration file ? (y/n) : "; cin >> yes_no;
        } while (yes_no != 'y' || yes_no != 'n');

        if (yes_no == 'y') {
            detected = false;
            kepala->loadINI(ini, "Head Controll");
            kepala->setInterruptMeasure(detected); // enable kalman + lowpass filter
            kepala->setInterruptMeasure(detected); // enable kalman + lowpass filter
        }

        cout << "\n Press ENTER to break \n";
        highLevel(degreeIncrement, delayStep, kalmanIteration);
        break;
    }
    default:
        break;
    }
    return 0;
}


void highLevel(uint16_t search_degreeIncrement, useconds_t search_delayStep,
               uint8_t kalmanIteration)
{
    cam.open(-1);
    ball = new MomentDetection();

    while (cv::waitKey(5) != '\n') {
        cam >>img;
        ball->threshold(&img);
        ball->moment(img);
        if (ball->getArea() != 0) {
            kepala->targetTracking(ball->getX(), ball->getY(), kalmanIteration);
            detected = true;
        }
        else {
            kepala->sinusoidalSearch(search_degreeIncrement, search_delayStep);
            detected = false;
        }
    }
}


void debug()
{
    cout << "[P:" << kepala->getPanAngle() << ", T:" << kepala->getTiltAngle() << "]\r";
}
