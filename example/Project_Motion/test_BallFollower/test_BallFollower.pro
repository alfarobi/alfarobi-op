TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lpthread -lserial -lrt \
-lopencv_core -lopencv_highgui -lopencv_imgproc

INCLUDEPATH += \
../../../motion/ \
../../../imgproc/ \
../../../std/minIni/ \
../../../std/math/ \
../../../

SOURCES += main.cpp \
    ../../../motion/head.cpp \
    ../../../motion/darwin/Walking.cpp \
    ../../../motion/darwin/MX28.cpp \
    ../../../motion/darwin/MotionStatus.cpp \
    ../../../motion/darwin/MotionManager.cpp \
    ../../../motion/darwin/LinuxMotionTimer.cpp \
    ../../../motion/darwin/LinuxCM730.cpp \
    ../../../motion/darwin/LinuxActionScript.cpp \
    ../../../motion/darwin/Kinematics.cpp \
    ../../../motion/darwin/JointData.cpp \
    ../../../motion/darwin/Head.cpp \
    ../../../motion/darwin/CM730.cpp \
    ../../../motion/darwin/Action.cpp \
    ../../../imgproc/momentdetection.cpp \
    ../../../std/minIni/minIni.c \
    ../../../std/math/linreg.cpp \
    ../../../std/math/Matrix.cpp \
    ../../../std/math/Plane.cpp \
    ../../../std/math/Point.cpp \
    ../../../std/math/Vector.cpp \
    ../../../motion/ugm_cxx.cpp
    ../../../std/math/Vector.cpp

HEADERS += \
    ../../../motion/subcontroller.h \
    ../../../motion/head.h \
    ../../../motion/darwin/Walking.h \
    ../../../motion/darwin/MX28.h \
    ../../../motion/darwin/MotionStatus.h \
    ../../../motion/darwin/MotionModule.h \
    ../../../motion/darwin/MotionManager.h \
    ../../../motion/darwin/LinuxMotionTimer.h \
    ../../../motion/darwin/LinuxCM730.h \
    ../../../motion/darwin/LinuxActionScript.h \
    ../../../motion/darwin/Kinematics.h \
    ../../../motion/darwin/JointData.h \
    ../../../motion/darwin/Head.h \
    ../../../motion/darwin/FSR.h \
    ../../../motion/darwin/CM730.h \
    ../../../motion/darwin/Action.h \
    ../../../imgproc/momentdetection.h \
    ../../../motion/ugm_cxx.h
    ../../../imgproc/momentdetection.h
