/* Ball Follower Without Landmark Detection
 *
 * Refference : darwin/BallFollower.cpp in function BallFollower::Proccess()
 */

#include <iostream>

#include <opencv2/highgui/highgui.hpp>
#include "momentdetection.h"

#include "head.h"
#include "darwin/LinuxCM730.h"
#include "darwin/CM730.h"
#include "darwin/Walking.h"
#include "darwin/MotionManager.h"
#include "darwin/LinuxMotionTimer.h"

#include "std/threading.h"

#include "ugm_cxx.h"

#define U2D_DEV_NAME        "/dev/ttyUSB0"
#define FILE_WALKING_CONFIG "walk.ini"

using namespace std;
using namespace cv;
using namespace Robot;
using namespace afr;

#define kepala Head::GetInstance()
MomentDetection *ball;
minIni *ini;
pthread_t thread_gerakan;

void initDarwin(CM730 *cm730);
void ballFollower(const double &pan_head, const double &tilt_head,
                  const double &range_pan = 360, const double &range_tilt = 360);


void* mainGerakan(void *arg)
{
    while (arg == NULL) { // forever
        ballFollower(kepala->getPanAngle(), kepala->getTiltAngle());
    }
    return NULL;
}

int main()
{
    VideoCapture cam;
    Mat src, dst;

    ball = new MomentDetection();
    ini = new minIni(FILE_WALKING_CONFIG);
    LinuxCM730 linux_cm730(U2D_DEV_NAME);
    CM730 cm730(&linux_cm730);

    cam.open(-1);
    initDarwin(&cm730);

    threadInitialize(thread_gerakan, mainGerakan, 50);

    while (waitKey(5) != '\n') {
        cam >> src;
        src.copyTo(dst);

        ball->threshold(&dst);
        ball->moment(dst);
        cout << "Moment Ball : "
             << " x=" << ball->getX()
             << " y=" << ball->getY()
             << " area=" << ball->getArea() << endl;
        if (ball->getArea() != 0)
            kepala->targetTracking(ball->getX(), ball->getY(), 3);
    }

    return 0;
}


void ballFollower(const double &pan_head, const double &tilt_head, const double &range_pan, const double &range_tilt)
{
    static double tilt_rasio, pan_rasio;

//    const double m_KickTopAngle = -5.0;
    const double m_KickRightAngle = -30.0;
    const double m_KickLeftAngle = 30.0;

    const double m_FollowMaxFBStep = 30.0;
    const double m_FollowMinFBStep = 5.0;
    const double m_FollowMaxRLTurn = 35.0;
//    const double m_FitFBStep = 3.0;           // for kicking
//    const double m_FitMaxRLTurn = 35.0;       // for kicking
    const double m_UnitFBStep = 0.3;
    const double m_UnitRLTurn = 1.0;

    static double m_GoalFBStep, m_GoalRLTurn;
    static double m_FBStep, m_RLTurn;

    /* Darwin Robotis algorithm (maybe? [based on source code]) mumet le gambar *_*
    /
    /      kick_angle (maju/mundur)
    /   \     \  o  /
    /    \rot  \   / rotasi
    /     \ki   \ /   kanan
    /  ----\-----O-----------
    / in Darwin they use head position to make rotation (pan) or walking forward/backward (tilt)
    */

    pan_rasio = pan_head/range_pan;
    tilt_rasio = tilt_head/range_tilt;

    // pada radius sudut kick -> jalan lurus sambil nyesuaikan kepala biar pandangannya lurus ke bola
    if (pan_head  > m_KickRightAngle && pan_head < m_KickLeftAngle) {
        m_GoalFBStep = m_FollowMaxFBStep * tilt_rasio;
        if (m_GoalFBStep < m_FollowMinFBStep) m_GoalFBStep = m_FollowMinFBStep;
        m_GoalRLTurn = m_FollowMaxRLTurn * pan_rasio;
    } else { // rotasi
        m_GoalFBStep = 0;
        m_GoalRLTurn = m_FollowMaxRLTurn * pan_rasio;
    }

    if (Walking::GetInstance()->IsRunning() == false) {
        m_FBStep = 0;
        m_RLTurn = 0;
        Walking::GetInstance()->X_MOVE_AMPLITUDE = m_FBStep;
        Walking::GetInstance()->A_MOVE_AMPLITUDE = m_RLTurn;
        Walking::GetInstance()->Start();
    } else {
        if (m_FBStep < m_GoalFBStep) m_FBStep += m_UnitFBStep;
        else if (m_FBStep > m_GoalFBStep) m_FBStep = m_GoalFBStep;
        Walking::GetInstance()->X_MOVE_AMPLITUDE = m_FBStep;

        if (m_RLTurn < m_GoalRLTurn) m_RLTurn += m_UnitRLTurn;
        else if (m_RLTurn > m_GoalRLTurn) m_RLTurn -= m_UnitRLTurn;
        Walking::GetInstance()->A_MOVE_AMPLITUDE = m_RLTurn;
    }
}

void initDarwin(CM730 *cm730)
{
    if(MotionManager::GetInstance()->Initialize(cm730) == false)
    {
        printf("Fail to initialize Motion Manager!\n");
    }
    MotionManager::GetInstance()->LoadINISettings(ini);
    Walking::GetInstance()->LoadINISettings(ini);

    MotionManager::GetInstance()->AddModule((MotionModule*)Walking::GetInstance());
    LinuxMotionTimer *motion_timer = new LinuxMotionTimer(MotionManager::GetInstance());
    motion_timer->Start();

    // activate
    Walking::GetInstance()->m_Joint.SetEnableBodyWithoutHead(true);
    MotionManager::GetInstance()->SetEnable(true);
}
