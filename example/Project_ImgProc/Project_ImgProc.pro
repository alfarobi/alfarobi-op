TEMPLATE = subdirs

INCLUDEPATH += \
../../ \
../../imgproc

SUBDIRS += \
    test_MomentDetection \
    test_CircleDetection \
    test_CameraCalibration

OTHER_FILES += \
    ../../imgproc/README.md
