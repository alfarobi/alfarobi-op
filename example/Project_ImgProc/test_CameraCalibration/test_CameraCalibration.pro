TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lopencv_core -lopencv_calib3d

INCLUDEPATH += \
../../../ \
../../../std/minIni \
../../../imgproc

SOURCES += main.cpp \
    ../../../imgproc/uvcdynctrl/cameracontroll.cpp \
    ../../../imgproc/cameracalibration.cpp

HEADERS += \
    ../../../imgproc/uvcdynctrl/cameracontroll.h \
    ../../../imgproc/cameracalibration.h

