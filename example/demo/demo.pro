TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXX += -std=gnu++11

LIBS += \
-lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_video \
-lserial -lpthread

INCLUDEPATH += \
../../ \
../../imgproc/ \
../../std

SOURCES += main.cpp \
    ../../imgproc/visualodometry.cpp \
    ../../imgproc/momentdetection.cpp \
    ../../imgproc/circledetection.cpp \
    ../../motion/head.cpp \
    ../../motion/action.cpp \
    ../../std/minIni/minIni.c \
    ../../motion/ugm_cxx.cpp \
    ../../sensor/cmps10.cpp \
    ../../remote/joystick.cpp

HEADERS += \
    ../../imgproc/visualodometry.h \
    ../../imgproc/momentdetection.h \
    ../../imgproc/circledetection.h \
    ../../motion/subcontroller.h \
    ../../motion/head.h \
    ../../motion/data_subcontroller.h \
    ../../motion/action.h \
    ../../std/threading.h \
    ../../std/speed_test.h \
    ../../std/logfile.h \
    ../../std/custom_math.h \
    ../../std/minIni/minIni.h \
    ../../data/RoboCupGameControll.h \
    ../../data/AlfarobiCommunicationProtocol.h \
    ../../data/SimplificatonReffereBox.h \
    ../../motion/ugm_cxx.h \
    ../../sensor/fusion.h \
    ../../sensor/cmps10.h \
    ../../remote/joystick.h
