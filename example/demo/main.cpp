#define LIMIT_CIRCLE_DETECTION      5
#define BALL_MAX_AREA   222
#define BALL_MIN_AREA   23
#define BALL_MAX_RADIUS 10
#define BALL_MIN_RADIUS 2

#define KICK_PAN                    10
#define KICK_TILT                   180
#define KICK_FRAME_CENTER_X         10
#define KICK_FRAME_CENTER_Y         10
#define KICK_FRAME_LEFTLEG_X_MAX    5
#define KICK_FRAME_LEFTLEG_X_MIN    2
#define KICK_FRAME_RIGHTLEG_X_MAX   30
#define KICK_FRAME_RIGHTLEG_X_MIN   20

#define RALAT_COMPASS   10

#define CONFIG_FILE     "config.ini"

#include <iostream>

#include "std/threading.h"
#include "std/speed_test.h"
#include "std/custom_math.h"
#include "imgproc/momentdetection.h"
#include "imgproc/circledetection.h"
#include "motion/head.h"
#include "motion/subcontroller.h"
#include "motion/action.h"

#include <opencv2/highgui/highgui.hpp>

#define forever         1
#define Loop(x)         while(x)
#define Do_Forever_Move while (arg == NULL)

#define DEFAULT_AFR_PARAM    1, 5000
#define MOMENT_DETECTION        true
#define CIRCLE_DETECTION        false

using namespace std;
using namespace cv;
using namespace afr;

pthread_t gerakan_t;
struct MinMax { int min,max; }frameDepth;

struct BallState {
    bool not_found;
    bool is_far;
    bool is_near;
    bool in_foot_range;
    bool ready_to_kick;
    int8_t circle_counter; bool fail_to_detect_circle = false;
}s_ball;
struct GoalState {
    bool not_found;
    bool in_search;
    bool probably_in_left;
    bool probably_in_right;
    bool probably_in_center;
    bool probably_in_above;
}s_goal;
struct RobotState {
    bool not_moved;
    bool head_moved;
}s_robot;

struct Position { double x,y,z; }frame;
uint16_t compassHeading, enemyGoal;
#define Cek_Kompas(goal)    !(constraintErr(compassHeading, goal, RALAT_COMPASS))

#define kepala  Head::GetInstance()
#define lakukan Action::GetInstance()
MomentDetection m_ball, m_goal;
CircleDetection m_circle;
minIni *ini;

void setPos(const uint32_t posX, const uint32_t posY, const uint32_t posZ, bool type);
void setBallState();
void setGoalState();

void cam_init(VideoCapture &cam);

void *actionStateMachine(void *arg)
{
    kepala->loadINI(ini, "Motion Head");
    kepala->setFrameXKalmanNoise(0.033, 0.01);
    kepala->setFrameYKalmanNoise(0.04, 0.02);
    // WARNING : if neccesery Kp, Kd of panning set to negative value

    Do_Forever_Move {
        if (s_ball.not_found) {
            kepala->sinusoidalSearch(1, 500);
            lakukan->action(AFR_ROT_KA_PA);
        }
        else {
            kepala->targetTracking(frame.x, frame.y);
            if (s_ball.is_far) {
                kepala->setTiltingPID(1/7, 1/14);
                kepala->setPanningPID(1/8, 1/16);
                lakukan->action(AFR_MAJU_CEPAT);
            }
            if (s_ball.is_near) {
                kepala->setTiltingPID(1/6, 1/12);
                kepala->setPanningPID(1/7, 1/14);
                lakukan->action(AFR_MAJU_LAMBAT);
            }
            if (s_ball.in_foot_range) {
                // cek kompas -> revolusi
                while (Cek_Kompas(enemyGoal)) {
                    if (compassHeading > enemyGoal)
                        lakukan->action(AFR_REV_KA);
                    else if (compassHeading < enemyGoal)
                        lakukan->action(AFR_REV_KI);
                    kepala->targetTracking(frame.x, frame.y);
                }

                // cek gawang -> revolusi | geser
                /* metode sweep
                s_goal.in_search = true;
                kepala->sweepRight(180, 10, 5000);
                kepala->sweepLeft(360, 20, 5000);
                s_goal.in_search = false;
                if (s_goal.probably_in_left)
                    lakukan->action(AFR_ROT_KI);
                if (s_goal.probably_in_right)
                    lakukan->action(AFR_ROT_KA);
                */

                // metode revolusi sambil ndangak
                s_goal.in_search = true;
                while (s_goal.not_found) {
                    kepala->nod(180, 5, 10000);
                    if (Cek_Kompas(enemyGoal+20)) lakukan->action(AFR_REV_KA);
                    if (Cek_Kompas(enemyGoal-20)) lakukan->action(AFR_REV_KI);
                }
                s_goal.in_search = false;

                if (s_ball.ready_to_kick) {
                    kepala->moveAtAngle(KICK_TILT, KICK_PAN);
                    // compare frame of leg with frame camera for kick
                    if (between(KICK_FRAME_LEFTLEG_X_MIN, frame.x, KICK_FRAME_LEFTLEG_X_MAX)) {
                        lakukan->action(AFR_TENDANG_KI);
                        kepala->panning(40);
                    }
                    if (between(KICK_FRAME_RIGHTLEG_X_MIN, frame.x, KICK_FRAME_RIGHTLEG_X_MAX)) {
                        lakukan->action(AFR_TENDANG_KA);
                        kepala->panning(40);
                    }
                    if (!(s_ball.not_found)) {
                        if (frame.x < KICK_FRAME_CENTER_X)
                            lakukan->action(AFR_GES_KA);
                        if (frame.x > KICK_FRAME_CENTER_Y)
                            lakukan->action(AFR_GES_KI);
                    }
                }
            }
        }
    }

    return NULL;
}

int main()
{
    VideoCapture cam;
    Mat src, dst;
    ini = new minIni(CONFIG_FILE);
    timespec s_main_t;

    cam_init(cam);
    port_init();

    m_ball.loadINISettings(ini, "Ball");
    m_circle.loadINISettings(ini, "Ball");
    m_goal.loadINISettings(ini, "Goal");

    //==================
    PARENT_TIMER_START;
    //==================

    threadInitialize(gerakan_t, actionStateMachine, 30);

    Loop(forever) {
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &s_main_t);
        cam >> src;

        src.copyTo(dst);

        // TODO : landmark detection (-> test_MomentDetection)
        // filter green color
        // use findContours() to detect hole

        m_ball.threshold(&dst);
        m_ball.moment(dst);
        cout << "Moment Ball : "
             << " x=" << m_ball.getX()
             << " y=" << m_ball.getY()
             << " area=" << m_ball.getArea() << endl;
        setPos(m_ball.getX(), m_ball.getY(), m_ball.getArea(), MOMENT_DETECTION);
        setBallState();

        // TODO : use timer for detecting enemy
        // each 5 second and the robot condition is not moving
        if (speedMeasure(s_main_t) > 5 && s_robot.not_moved) {
            // do optical flow
        }

        while ((s_ball.is_near || s_ball.in_foot_range) && !(s_ball.fail_to_detect_circle)) {
            // WARNING : difficult calibration for HoughCircles();
            m_circle.threshold(&dst);
            if (m_circle.houghTransform(&dst) != 0)
                s_ball.fail_to_detect_circle = false;
            else {
                s_ball.circle_counter++;
                if (s_ball.circle_counter > LIMIT_CIRCLE_DETECTION)
                    s_ball.fail_to_detect_circle = true;
                break;
            }
            cout << "Circle Ball : "
                 << " x=" << m_circle.getX()
                 << " y=" << m_circle.getY()
                 << " radius=" << m_circle.getRadius() << endl;
            setPos(m_circle.getX(), m_circle.getY(), m_circle.getRadius(), CIRCLE_DETECTION);
            setBallState();
            cam >> dst;
        }
        s_ball.fail_to_detect_circle = false;

        while (s_goal.in_search) {
            m_goal.threshold(&dst);
            m_goal.moment(dst);
            cout << "Moment Ball : "
                 << " x=" << m_goal.getX()
                 << " y=" << m_goal.getY()
                 << " area=" << m_goal.getArea() << endl;
            setPos(m_goal.getX(), m_goal.getY(), m_goal.getArea(), MOMENT_DETECTION);
            setGoalState();
            cam >> dst;
        }
    }

    return 0;
}

void setPos(const uint32_t posX, const uint32_t posY, const uint32_t posZ, bool type)
{
    frame.x = posX; frame.y = posY; frame.z = posZ;
    if (type) {
        frameDepth.min = BALL_MIN_AREA;
        frameDepth.max = BALL_MAX_AREA;
    } else {
        frameDepth.min = BALL_MIN_RADIUS;
        frameDepth.max = BALL_MAX_RADIUS;
    }
}

// TODO : dead reckoning based on image pixel size
void setBallState()
{
    if ((frame.x == 0 && frame.y == 0) || frame.z == 0)
        s_ball.not_found = true;
    else {
        if (frame.z >= frameDepth.max) {
            s_ball.is_far = true;
            s_ball.is_near = false;
            s_ball.in_foot_range = false;
        }
        else if (frame.z >= frameDepth.min) {
            s_ball.is_near = true;
            s_ball.is_far = false;
            s_ball.in_foot_range = false;
        }
        else if (frame.z <= frameDepth.min) {
            s_ball.in_foot_range = true;
            s_ball.is_near = false;
            s_ball.is_far = false;
        }
    }
}

void setGoalState()
{
    static bool goalie[3];

    if (s_goal.in_search) {
        if (frame.z != 0) goalie[0] = true;
        if (frame.z == 0 && goalie[0]) goalie[1] = true;
        if (frame.z != 0 && goalie[1]) goalie[2] = true;
        if (goalie[0] && goalie[1] && goalie[2]) s_goal.probably_in_center = true;
        if (kepala->getPanAngle() == 360)
            if (goalie[0]) s_goal.probably_in_above = true;
        // NOTE : bug maybe in here !
        if (goalie[0]) s_goal.probably_in_right = true;
    }
}

void cam_init(VideoCapture &cam)
{
    cam.set(CV_CAP_PROP_FRAME_WIDTH, 640);
    cam.set(CV_CAP_PROP_FRAME_HEIGHT, 480);

    if (!cam.open(-1)) {
        cerr << "Error : Couldn't open camera !\n";
        exit(1);
    }
}
