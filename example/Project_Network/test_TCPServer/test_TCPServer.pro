TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += \
../../../network/ \
../../../

SOURCES += main.cpp \
    ../../../network/TCP.cpp

HEADERS += \
    ../../../network/TCP.h

