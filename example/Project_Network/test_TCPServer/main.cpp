/* TCP Server
 *
 * check each funciton to understanding this example
 */

#include <iostream>
#include <vector>
#include <stdint-gcc.h>
#include "TCP.h"

using namespace afr;
using namespace std;

//============Configuration============
#define PORT 3838
//=====================================

struct DataPackage {
    const char header[4] = {'T','e','h','e'};
    int id;
    uint8_t data8bit;
    uint16_t data16bit;
    uint32_t data32bit;
    char name[18];
};

vector<TCPServer> sockets;
TCPServer server;

void createSocket(uint &n_socket);
void communicationVer1(uint &number_socket);
void communicationVer2(uint &number_socket);
void printDataPackage(DataPackage &package);

int main()
{
    int choice = 0;
    uint n_sokcet, id_sokcet;

    cout << "[Establish socket ...]\n"
         << "how many client do you want to establish ? : ";
    cin >> n_sokcet;
    cout << endl;

    createSocket(n_sokcet);
    while (1) {
        cout << "Please choose !\n==========================================\n"
             << "1. Communication version 1 (string version) \n"
             << "2. Communication version 2 (package data / struct version) \n"
             << "3. EXIT \n\n"
             << "your choice ? : ";
        cin >> choice;
        cout << "\n for client number : ";
        cin >> id_sokcet;
        cout << endl;

        switch (choice) {
        case 1:
            communicationVer1(id_sokcet);
            break;
        case 2:
            communicationVer2(id_sokcet);
            break;
        case 3:
            return 3;
        default:
            break;
        }
    }
    return 0;
}

void createSocket(uint &n_socket)
{
    // create connection
    try {
        server = TCPServer(PORT);

        cout << "[Waiting...]" << endl;

        for (uint i = 0; i < n_socket; ++i)
            server.accept(sockets[i]);

        cout << "[Accepted!!!]" << endl;
    } catch (TCPSocketException& err) {
        cerr << err.description() << endl;
    }
}

void communicationVer1(uint &number_socket)
{
    try {
        string packet_send, packet_recieve;

        // ====send=====
        cout << "Your name : ";
        cin >> packet_send;
        sockets[number_socket-1] << "\nHello my name is : " << packet_send;

        // =====recieve======
        sockets[number_socket-1] >> packet_recieve;
        cout << "Client Reply : " << packet_recieve << endl;
    } catch (TCPSocketException& err) {
        cerr << err.description() << endl;
    }
}

void communicationVer2(uint &number_socket)
{
    try {
        // ======initialize value of data to send======
        DataPackage server_data, client_data;
        server_data.id = number_socket;
        server_data.data8bit = 200+number_socket;
        server_data.data16bit = 4030+number_socket;
        server_data.data32bit = 32760+number_socket;
        cout << "\nwhat its name (max 18 char) : ";
        cin >> server_data.name;
        //=============================================

        cout << "[Sending....]" << endl;
        sockets[number_socket-1].send(Struct2String(server_data), sizeof(server_data));

        cout << "[Recieving....]" << endl;
        sockets[number_socket-1].recv(Struct2String(client_data), sizeof(server_data));

        printDataPackage(client_data);
    } catch (TCPSocketException& err) {
        cerr << err.description() << endl;
    }
}

void printDataPackage(DataPackage &package)
{
    cout << package.header << endl
         << "socket id : " << package.id << endl
         << "data 8 bit" << (int) package.data8bit << endl
         << "data 8 bit" << (int) package.data16bit << endl
         << "data 8 bit" << (int) package.data32bit << endl
         << "name : " << package.name << "\n end of package " << package.header << endl;
}
