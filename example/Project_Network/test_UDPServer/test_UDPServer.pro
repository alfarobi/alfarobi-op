TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += \
../../../network/ \
../../../

SOURCES += main.cpp \
    ../../../network/UDP.cpp

HEADERS += \
    ../../../network/UDP.h

