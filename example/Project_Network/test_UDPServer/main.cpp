#include <iostream>
#include "UDP.h"

using namespace afr;
using namespace std;

//============Configuration============
#define INTERFACE           "wlan0"
#define BROADCAST_IP_PORT   "192.168.1.255:3838"
#define BROADCAST_IP        "192.168.1.255"
#define PORT                3838
//=====================================

struct DataPackage {
    const char header[4] = {'Y','e','h','e'};
    int id = 1;
    uint8_t data8bit;
    uint16_t data16bit;
    uint32_t data32bit;
    char name[18];
};

UDPServer server;

void communicationVer1();
void communicationVer2();

int main()
{
    int choice = 0;

    cout << "[Establish connection ...]\n";
    try {
//        server = UDPServer(BROADCAST_IP_PORT, INTERFACE);
        server = UDPServer(BROADCAST_IP, PORT, INTERFACE);
    } catch (UDPSocketException &err) {
        cerr << err.description() << endl;
    }

    cout << "Please choose !\n==========================================\n"
         << "1. Communication version 1 (string version) \n"
         << "2. Communication version 2 (package data / struct version) \n"
         << "3. EXIT \n\n"
         << "your choice ? : ";
    cin >> choice;
    while (1) {
        switch (choice) {
        case 1:
            communicationVer1();
            break;
        case 2:
            communicationVer2();
            break;
        case 3:
            return 3;
        default:
            break;
        }
    }

    return 0;
}


void communicationVer1()
{
    try {
        string packet_send;

        // send
        cout << "Your name : ";
        cin >> packet_send;
        server << "\nHello my name is : " << packet_send;
    } catch (UDPSocketException& err) {
        cerr << err.description() << endl;
    }
}


void communicationVer2()
{
    try {
        // ======initialize value of data to broadcast======
        DataPackage server_data;
        server_data.data8bit = 200+server_data.id;
        server_data.data16bit = 4030+server_data.id;
        server_data.data32bit = 32760+server_data.id;
        cout << "\nwhat its name (max 18 char) : ";
        cin >> server_data.name;
        //=============================================


        cout << "[Sending....]" << endl;
        server.send(Struct2String(server_data), sizeof(server_data));

    } catch (UDPSocketException& err) {
        cerr << err.description() << endl;
    }
}
