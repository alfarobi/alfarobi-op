#include <iostream>
#include "UDP.h"

using namespace afr;
using namespace std;

//============Configuration============
#define IP_PORT_Server  "192.168.1.1:3838"
#define IP      "192.168.1.1"
#define PORT    3838
//=====================================

struct DataPackage {
    const char header[4] = {'K','e','h','e'};
    int id = 1;
    uint8_t data8bit;
    uint16_t data16bit;
    uint32_t data32bit;
    char name[18];
};

UDPClient client;

void communicationVer1();
void communicationVer2();
void printDataPackage(DataPackage &package);

int main()
{
    int choice;

    cout << "[Establish socket ...]\n";
    try {
//        client = UDPClient(IP_PORT_Server);
        client = UDPClient(IP, PORT);
    } catch (UDPSocketException &err) {
        cerr << err.description() << endl;
    }

    cout << "Please choose !\n==========================================\n"
         << "1. Communication version 1 (string version) \n"
         << "2. Communication version 2 (package data / struct version) \n"
         << "3. EXIT \n\n"
         << "your choice ? : ";
    cin >> choice;
    while (1) {
        switch (choice) {
        case 1:
            communicationVer1();
            break;
        case 2:
            communicationVer2();
            break;
        case 3:
            return 3;
        default:
            break;
        }
    }
    return 0;
}


void communicationVer1()
{
    // create connection
    try {
        string packet_recieve;

        // recv
        client >> packet_recieve;
        cout << "Server Reply : " << packet_recieve << endl;
    } catch (UDPSocketException& err) {
        cerr << err.description() << endl;
    }
}


void communicationVer2()
{
    try {
        DataPackage server_data;

        cout << "[Recieving....]" << endl;
        client.recv(Struct2String(server_data), sizeof(server_data));

        printDataPackage(server_data);
    } catch (UDPSocketException& err) {
        cerr << err.description() << endl;
    }
}


void printDataPackage(DataPackage &package)
{
    cout << package.header << endl
         << "socket id : " << package.id << endl
         << "data 8 bit" << (int) package.data8bit << endl
         << "data 8 bit" << (int) package.data16bit << endl
         << "data 8 bit" << (int) package.data32bit << endl
         << "name : " << package.name << "\n end of package " << package.header << endl;
}
