/* Get Data form ReffereBox and Simplify It
 *
 */

#include <iostream>
#include "UDP.h"
#include "simplificationData_ReffereBox.h"

// ============== Configuration====================
#define IP_PORT_GameController  "192.168.1.1:3838"

#define IP      "192.168.1.1"
#define PORT    3838
//==================================================

using namespace std;
using namespace afr;


RoboCupGameControlData data_wasit;
UDPClient socket_GameController;

void printData(RoboCupGameControlData &data);

int main()
{
    /* =================create UDP socket connection======================
     *(choose one you like / comment you don't want to use)
     */
    try {
//        socket_GameController = UDPClient(IP_PORT_GameController);
        socket_GameController = UDPClient(IP, PORT);
    } catch (UDPSocketException &err) {
        cerr << err.description() << endl;
    }
    //=====================================================================

    while (1) {
        if (socket_GameController.recv(Struct2String(data_wasit), sizeof(data_wasit))) { // recieve data
            printData(data_wasit);
            cout << "Alfarobi\n========\n"
                 << "Simplification status : " << getStatusReffere(data_wasit) << endl
                 << "Target Goal : " << getTargetGoal(data_wasit) << endl;
        } else
            cerr << "Can't receieve data ! \n";
    }

    return 0;
}


void printData(RoboCupGameControlData &data)
{
    cout << data.header << " ver " << data.version << endl
         << "Player per team : " << (int)data.playersPerTeam << endl
         << "State : " << (int)data.state << endl
         << "Babak : " << data.firstHalf << endl
         << "Kick off team : " << (int)data.kickOffTeam << endl
         << "Secondary state : " << (int)data.secondaryState << endl
         << "Team cause drop in : " << (int)data.dropInTeam << " & time elapse after drop in is " << data.dropInTime << " seconds" << endl
         << "Time remaining : " << data.secsRemaining << " seconds" << endl;

    cout << "Team Cyan\n==========\n"
         << "\t No. Team : " << (int)data.teams[TEAM_CYAN].teamNumber << endl
         << "\t Colour ID : " << (int)data.teams[TEAM_CYAN].teamColour << endl
         << "\t Goal Colour ID : " << (int)data.teams[TEAM_CYAN].goalColour << endl
         << "\t Score : " << (int)data.teams[TEAM_CYAN].score << endl;
    for (int i = 0; i < 3; ++i) {
        cout << "\t Player " << i << endl
             << "\t\t penalty state : " << (int)data.teams[TEAM_CYAN].players[i].penalty << endl
             << "\t\t time till unpenalized : " << (int)data.teams[TEAM_CYAN].players[i].secsTillUnpenalised << " seconds" << endl;
    }

    cout << "Team Magenta\n==========\n"
         << "\t No. Team : " << (int)data.teams[TEAM_MAGENTA].teamNumber << endl
         << "\t Colour ID : " << (int)data.teams[TEAM_MAGENTA].teamColour << endl
         << "\t Goal Colour ID : " << (int)data.teams[TEAM_MAGENTA].goalColour << endl
         << "\t Score : " << (int)data.teams[TEAM_MAGENTA].score << endl;
    for (int i = 0; i < 3; ++i) {
        cout << "\t Player " << i << endl
             << "\t\t penalty state : " << (int)data.teams[TEAM_MAGENTA].players[i].penalty << endl
             << "\t\t time till unpenalized : " << (int)data.teams[TEAM_MAGENTA].players[i].secsTillUnpenalised << " seconds" << endl;
    }
}
