TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += \
../../../network/ \
../../../

SOURCES += main.cpp \
    ../../../network/UDP.cpp \
    ../../../network/simplificationData_ReffereBox.cpp

HEADERS += \
    ../../../network/UDP.h \
    ../../../network/simplificationData_ReffereBox.h

