/* TCP CLient
 *
 * check each funciton to understanding this example
 */

#include <iostream>
#include <stdint-gcc.h>
#include "TCP.h"

using namespace std;
using namespace afr;

//============Configuration============
#define IP_SERVER   "192.168.1.1"
#define PORT        3838
//=====================================

struct DataPackage {
    const char header[4] = {'H','e','h','e'};
    int id = 1;
    uint8_t data8bit;
    uint16_t data16bit;
    uint32_t data32bit;
    char name[18];
};

TCPClient client;

void createSocket();
void communicationVer1();
void communicationVer2();
void printDataPackage(DataPackage &package);

int main()
{
    int choice;

    cout << "[Establish socket ...]\n";
    createSocket();

    while (1) {
        cout << "Please choose !\n==========================================\n"
             << "1. Communication version 1 (string version) \n"
             << "2. Communication version 2 (package data / struct version) \n"
             << "3. EXIT \n\n"
             << "your choice ? : ";
        cin >> choice;
        cout << endl;

        switch (choice) {
        case 1:
            communicationVer1();
            break;
        case 2:
            communicationVer2();
            break;
        case 3:
            return 3;
        default:
            break;
        }
    }

    return 0;
}


void createSocket()
{
    // create connection
    try {
        client = TCPClient(PORT);

        cout << "[Waiting...]" << endl;
        client.connect(IP_SERVER, PORT);
        cout << "[Connected!!!]" << endl;
    } catch (TCPSocketException &err) {
        cout << err.description() << endl;
    }
}


void communicationVer1()
{
    try {
        string packet_send, packet_recieve;

        // ====send=====
        cout << "Your name : ";
        cin >> packet_send;
        client << "\nHello my name is : " << packet_send;

        // =====recieve======
        client >> packet_recieve;
        cout << "Server Reply : " << packet_recieve << endl;
    } catch (TCPSocketException& err) {
        cerr << err.description() << endl;
    }
}


void communicationVer2()
{
    try {
        // ======initialize value of data to send======
        DataPackage server_data, client_data;
        client_data.data8bit = 200+client_data.id;
        client_data.data16bit = 4030+client_data.id;
        client_data.data32bit = 32760+client_data.id;
        cout << "\nwhat its name (max 18 char) : ";
        cin >> client_data.name;
        //=============================================

        cout << "[Sending....]" << endl;
        client.send(Struct2String(client_data), sizeof(client_data));

        cout << "[Recieving....]" << endl;
        client.recv(Struct2String(server_data), sizeof(server_data));

        printDataPackage(server_data);
    } catch (TCPSocketException& err) {
        cerr << err.description() << endl;
    }
}


void printDataPackage(DataPackage &package)
{
    cout << package.header << endl
         << "socket id : " << package.id << endl
         << "data 8 bit" << (int) package.data8bit << endl
         << "data 8 bit" << (int) package.data16bit << endl
         << "data 8 bit" << (int) package.data32bit << endl
         << "name : " << package.name << "\n end of package " << package.header << endl;
}
