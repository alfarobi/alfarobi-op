/* u(Micro) Gateway Motion - C10
 *
 * Author : Alfarobi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "ugm_cxx.h"
#include <sstream>
#include <iostream>

using namespace afr;
using namespace LibSerial;
using namespace std;

uGM_Cxx::uGM_Cxx()
{
    serial = new SerialStream();
}

int uGM_Cxx::autoSearch(uint32_t n, int except)
{
    static stringstream ss;
    ss << "/dev/ttyUSB";
    for (int i = 0; i < n; ++i) {
        if (i != except) {
            ss << i;
            if (open(ss.str()) && checkVersion()) return i;
            ss.str(string()); // clear the stream
            serial->Close();
        }
    }
    return -1;
}

void uGM_Cxx::open(SerialStream *serial)
{
    delete serial;
    this->serial = serial;
}

bool uGM_Cxx::open(const std::string &dev)
{
    serial = new SerialStream();

    serial->Open(dev);
    if(!serial->good()) {
        cerr << "Error: Could not open serial device !"
             << endl;
        return false;
    }

    serial->SetBaudRate(SerialStreamBuf::BAUD_57600);
    if ( ! serial->good() ) {
        cerr << "Error: Could not set baudrate" <<
                endl ;
        return false ;
    }

    serial->SetCharSize(SerialStreamBuf::CHAR_SIZE_DEFAULT);
    if ( ! serial->good() ) {
        cerr << "Error: Could not set the character size." <<
                endl ;
        return false ;
    }

    serial->SetParity(SerialStreamBuf::PARITY_DEFAULT);
    if ( ! serial->good() ) {
        cerr << "Error: Could not set the parity" <<
                endl ;
        return false ;
    }

    serial->SetNumOfStopBits(1);
    if ( ! serial->good() ) {
        cerr << "Error: Could not set the stop bit" <<
                endl ;
        return false ;
    }

    serial->SetFlowControl(SerialStreamBuf::FLOW_CONTROL_NONE);
    if ( ! serial->good() ) {
        cerr << "Error: Could not set the flow control." <<
                endl;
        return false ;
    }

    serial->SetVMin(0);
    serial->SetVTime(5);
    return true;
}

bool uGM_Cxx::isOpen()
{
    return serial->IsOpen();
}

bool uGM_Cxx::checkVersion()
{
    buf = recvData();
    return (buf.version == uGM_VERSION);
}

void uGM_Cxx::close()
{
    serial->Close();
}

void uGM_Cxx::sendData(uGM_Cxx_Write_StructData data)
{
    serial->write((char*)&data, sizeof(data));
}

uGM_Cxx_Read_StructData uGM_Cxx::recvData()
{
    serial->read((char*)&buf, sizeof(buf));
    return buf;
}


/*
uint8_t uGM_Cxx::readByte(int address)
{
    static char buf;
    serial->read(&buf, 1);
    return buf;
}

uint16_t uGM_Cxx::readWord(int address)
{
}

bool uGM_Cxx::checkVersion()
{
    return (readByte(CM_ID) == uGM_VERSION);
}

void uGM_Cxx::writeByte(int address, char value)
{
    serial->write(&value, 1);
}

void uGM_Cxx::writeWord(int address, uint16_t value)
{
}
*/
