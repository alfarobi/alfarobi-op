#ifndef DATA_SUBCONTROLLER_H
#define DATA_SUBCONTROLLER_H

// TODO : dead reckoning ardone
// TODO : adding my autoperspective library
// TODO : visual odometry (optical flow)

//define nomor gerakan motion
#define AFR_STANDBY      1
#define AFR_MAJU_CEPAT	2
#define AFR_MAJU_LAMBAT	3
#define AFR_GES_KA       4
#define AFR_GES_KI       5
#define AFR_ROT_KA 		6
#define AFR_ROT_KI 		7
#define AFR_TENDANG_KA	8
#define AFR_TENDANG_KI	9
#define AFR_BELOK_KA     10
#define AFR_BELOK_KIRI	11
#define AFR_REV_KA		12
#define AFR_REV_KI		13
#define AFR_STEP_KA		14
#define AFR_STEP_KI		15
#define AFR_ROT_KA_PA	16
#define	AFR_ROT_KI_PA	17
#define AFR_GES_KA_PA	18
#define AFR_GES_KI_PA	19
#define AFR_BALANCING    49
#define AFR_STOP         50

#endif // DATA_SUBCONTROLLER_H
