/* Head Controll
 *
 * Author : Alfarobi
 *
 * Status : NOT Tested
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef HEAD_H
#define HEAD_H

// ========== CONFIGURE ===========
#define HEAD_PAN_MAX    1024        /*!< fixed configuration for maximum value of head being rotate along \p y axis. */
#define HEAD_PAN_MIN    512         /*!< fixed configuration for minimum value of head being rotate along \p y axis. */
#define HEAD_TILT_MAX   1024        /*!< fixed configuration for maximum value of head being rotate along \p x axis. */
#define HEAD_TILT_MIN   512         /*!< fixed configuration for minimum value of head being rotate along \p x axis. */

#define HEAD_THREADED true          /*!< fixed configuration for how you implement this class
                                    usefull for Head::sinusoidalSearch() */
// ================================

#include <stdint-gcc.h>
#include "../std/minIni/minIni.h"
#include "../std/logfile.h"
#include "ugm_cxx.h"

namespace afr {

/*!
 * \brief Class for Controll the movement of Head
 *
 *  You can't make instance object. However you can get instance of this class.
 *  \n Example how to use :
 *  \code
 *      Head::GetInstance()->cosinusMove(180.78, 2);
 *  \endcode
 *  \n or you can simplify it like :
 *  \code
 *      #define head_obj    Head::GetInstance()
 *      int main()
 *      {
 *          head_obj->sinusoidalSearch(6.85, 10000);
 *      }
 *  \endcode
 *
 * \sa uGM_Cxx
 */
class Head
{
public:
    static Head* GetInstance() { return m_UniqueInstance; } /*!< this class is link list. */

    // high level
    void targetTracking(double frame_x, double frame_y, const uint8_t kalman_iteration = 1);
    void sinusoidalSearch(const double &degree_increment, const useconds_t &delay_steps);

    // medium level
    void nod(const double &panAngle_offset, const double &tiltAngle_increment, const useconds_t delay_steps);
    void sweepLeft(const double &tiltAngle_offset, const double &panAngle_increment, const useconds_t delay_step);
    void sweepRight(const double &tiltAngle_offset, const double &panAngle_increment, const useconds_t delay_step);

    // low level
    void panning(const double angle);
    void tilting(const double angle);
    void moveAtAngle(const double tiltAngle, const double panAngle);
    void cosinusMove(const double degree, const int multiplier);

    // setting filter controll for targetTracking();
    void setPanningPID(const double P_gain, const double D_gain) { this->P_gain.pan = P_gain; this->D_gain.pan = D_gain; }
    void setTiltingPID(const int P_gain, const int D_gain) { this->P_gain.tilt = P_gain; this->D_gain.tilt = D_gain; }
    // TODO : reduce kalman noise parameter
    void setFrameXKalmanNoise(const double Q, const double R) { P_noise.frame_x = Q; K_noise.frame_x = R; }
    void setFrameYKalmanNoise(const double Q, const double R) { P_noise.frame_y = Q; K_noise.frame_y = R; }
    // TODO : change dt/dt+RC param to cut-off frequency
    void setLowPassAlpha(const double dt, const double RC) { this->dt = dt; this->RC = RC; lowPass_once = true; }

    // setting interupt flag for high end medium level (except targetTracking())
    void setInterruptState(bool &flag) { flag_INT = &flag; }
    void setInterruptMeasure(bool &flag) { flag_measure = &flag; }

    // data
    double getPanAngle() const { return (pan-HEAD_PAN_MIN)*360/raw_pan_size; }
    double getTiltAngle() const { return (tilt-HEAD_TILT_MIN)*360/raw_tilt_size; }

    void loadINI(minIni *ini, const std::string &section);
    void saveINI(minIni *ini, const std::string &section);

    void logDataEnable(const std::string &filename, const double &each_seconds);
    void open(uGM_Cxx &controller);

private:
    static Head* m_UniqueInstance;
    Head();

    uGM_Cxx *controller;
    bool controller_enable;

    LogFile *log;
    double time_gap;

    void logData();

    uint32_t pan, tilt;
    uint32_t raw_pan_size, raw_tilt_size;
    bool *flag_INT;

    uint16_t centerFrame_x, centerFrame_y;

    // PIDControll() variable
    struct PID { double pan, tilt; };
    PID P_gain, D_gain;
    int err, lastErr, sumErr;

    // kalmanFilter() variable
    struct WhiteNoise { double frame_x, frame_y; };
    WhiteNoise P_noise, K_noise;
    double P_last;
    double raw_est, raw_est_last;
    double error_kalman;
    bool *flag_measure;

    // lowPassFilter() variable
    double dt, RC;
    bool lowPass_once;

    uint32_t degree2raw(const double degree, bool type);
    double kalmanFilter(double measured_val, const uint8_t iteration, const double Q, const double R, bool begin_measure);
    int PIDControll(const double measured_val, const double ideal_val, const double Kp, const double Kd, const double Ki = 0);
    double lowPassFilter(const double measured_val, const double dt, const double RC, bool begin_measure = true);

    void write(uint32_t &command);
};

}

#endif // HEAD_H
