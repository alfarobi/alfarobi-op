/* CM-5xx Alfarobi version Task Program
 *
 * Author : Alfarobi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "action.h"
#include "subcontroller.h"

using namespace afr;

Action* Action::m_UniqueInstance = new Action();

/*!
 * \brief usefull in multi-thread program
 * \param INT
 */
void Action::setInterrupt(bool *INT)
{
    this->INT = INT;
}

/*!
 * \brief accesing the action
 * \param acts
 * \param asMany_N_acts
 * \param delay_steps
 */
void Action::action(uint32_t acts, uint8_t asMany_N_acts, useconds_t delay_steps)
{
    lastStatus = acts;
    for(uint8_t i=0;i<asMany_N_acts;i++) {
        port_write(acts);
        usleep(delay_steps);
    }
}
