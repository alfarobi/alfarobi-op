/* CM-5xx Alfarobi version Task Program
 *
 * Author : Alfarobi
 *
 * Status : NOT Tested
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef ACTION_H
#define ACTION_H

#include <stdint-gcc.h>
#include <unistd.h>
#include "data_subcontroller.h"

namespace afr {

/*!
 * \brief Class for accesing predefined action in CM-5xx
 *      that using Alfarobi version \b Task \b Program
 * \sa data_subcontroller.h
 */
class Action
{
public:
    static Action* GetInstance() { return m_UniqueInstance; }

    void setInterrupt(bool *INT);
    void action(uint32_t acts, uint8_t asMany_N_acts = 1, useconds_t delay_steps = 5000);

private:
    static Action* m_UniqueInstance;

    uint8_t lastStatus;
    bool *INT;
};

}

#endif // ACTION_H
