/* u(Micro) Gateway Motion - C10
 *
 * Author : Alfarobi
 *
 * Status : NOT Finished
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef uGM_Cxx_H
#define uGM_Cxx_H

#include <stdint-gcc.h>
#include <string>
#include <SerialStream.h>

namespace afr
{

#define uGM_VERSION 10

struct uGM_Cxx_Write_StructData
{
    bool led1, led2, led3, led4;
    double tilt, pan;
};

struct uGM_Cxx_Read_StructData
{
    short version = uGM_VERSION;
    bool button1, button2, button3, button4;
    double acc, gyro, magneto;
    double pitch, roll, yaw;
};

/*!
 * \brief Class for accessing uGM_Cxx subcontroller
 */
class uGM_Cxx
{
    /*
public:
    enum
    {
        CM_ID = 1
    };

    enum
    {
        HEAD_X = 50,
        HEAD_Y = 51
    };

    enum
    {
        CMPS10_VERSION = 0,
        CMPS10_HEAD_BYTE = 1,
        CMPS10_HEAD_H = 2,
        CMPS10_HEAD_L = 3,
        CMPS10_PITCH_BYTE = 4,
        CMPS10_ROLL_BYTE = 5,
        CMPS10_MAGNETO_X_H = 10,
        CMPS10_MAGNETO_X_L =11,
        CMPS10_MAGNETO_Y_H = 12,
        CMPS10_MAGNETO_Y_L = 13,
        CMPS10_MAGNETO_Z_H = 14,
        CMPS10_MAGNETO_Z_L = 15,
        CMPS10_ACC_X_H = 16,
        CMPS10_ACC_X_L = 17,
        CMPS10_ACC_Y_H = 18,
        CMPS10_ACC_Y_L = 19,
        CMPS10_ACC_Z_H = 20,
        CMPS10_ACC_Z_L = 21
    };
    */

public:
    uGM_Cxx();
    int autoSearch(uint32_t n, int except = -1);
    void open(LibSerial::SerialStream *serial);
    bool open(const std::string &dev);
    void close();

    //========== caranya darwin =============
    //    uint8_t readByte(int address);
    //    uint16_t readWord(int address);
    //    void writeByte(int address, char value);
    //    void writeWord(int address, uint16_t value);
    //======================================

    void sendData(uGM_Cxx_Write_StructData data);
    uGM_Cxx_Read_StructData recvData();

    bool isOpen();

private:
    LibSerial::SerialStream *serial;

    uGM_Cxx_Read_StructData buf;
    bool checkVersion(); // for autoSearch()
};

}

#endif // uGM_Cxx_H
