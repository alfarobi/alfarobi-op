/* Head Controll
 *
 * Author : Alfarobi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "head.h"
#include "subcontroller.h"
#include <cmath>

#define SPEED   1
#define HEAD_SECTION    "Motion Head"

#define PAN_CONV    false
#define TILT_CONV   true

using namespace afr;

#if HEAD_THREADED == false
static int count;
#endif

Head* Head::m_UniqueInstance = new Head();
static bool log_once = true;
//static bool kalman_once = false;
static bool default_flag_measure = false;


Head::Head()
{
    raw_pan_size = HEAD_PAN_MAX - HEAD_PAN_MIN;
    raw_tilt_size = HEAD_TILT_MAX - HEAD_TILT_MIN;
    lastErr = 0;
    raw_est_last = 0;
    P_last = 0;
    flag_measure = &default_flag_measure;
    controller_enable = false;
}

/*!
 * \brief track to certain position based on camera frame
 * \param frame_x position along x axis of frame
 * \param frame_y position along y axis of frame
 * \param kalman_iteration how many iteration you want to do with kalman filter.
 *      \n higher the value higher the accuration but lowering the proccess speed
 *      \n set 0 to disable kalman filter
 */
void Head::targetTracking(double frame_x, double frame_y, const uint8_t kalman_iteration)
{
    // TODO : use PID Darwin
    /*
     * BallTracker.cpp -> pixel per image
     *
     */
    // PID jimmy
    frame_y = kalmanFilter(frame_y, kalman_iteration, P_noise.frame_y, K_noise.frame_y, *flag_measure);
    frame_y = lowPassFilter(frame_y, dt, RC, *flag_measure);
    tilt += PIDControll(frame_y, centerFrame_y, P_gain.pan, D_gain.pan);
    if (tilt > HEAD_TILT_MAX) tilt = HEAD_TILT_MAX;
    else if (tilt < HEAD_TILT_MIN) tilt = HEAD_TILT_MIN;
    this->write(tilt);

    frame_x = kalmanFilter(frame_x, kalman_iteration, P_noise.frame_x, K_noise.frame_x, *flag_measure);
    frame_x = lowPassFilter(frame_x, dt, RC, *flag_measure);
    pan += PIDControll(frame_x, centerFrame_x, P_gain.tilt, D_gain.tilt);
    if (pan > HEAD_PAN_MAX) pan = HEAD_PAN_MAX;
    else if (pan < HEAD_PAN_MIN) pan = HEAD_PAN_MIN;
    this->write(pan);
}

/*!
 * \brief search along with movement like sinusoidal pattern
 * \param degree_increment how many degree for each move
 * \param delay_steps delay for each move
 */
void Head::sinusoidalSearch(const double &degree_increment, const useconds_t &delay_steps)
{
#if HEAD_THREADED == true
    for (double i = 0; i <= 360; i += degree_increment) {
        if (*flag_INT) break;
        cosinusMove(i, 2);
        usleep(delay_steps);
    }
    for (double i = 360; i >= 0; i -= degree_increment) {
        if (*flag_INT) break;
        cosinusMove(i, -2);
        usleep(delay_steps);
    }
#endif
#if HEAD_THREADED == false
    if (count <= 360) {
        cosinusMove(count, 2);
        count += degree_increment;
    }
    if (count >= 0) {
        cosinusMove(count, -2);
        count += degree_increment;
    }
#endif
}

/*!
 * \brief move form left to right
 * \param tiltAngle_offset set \p y position of head
 * \param panAngle_increment how many degree for each move along \p x axis
 * \param delay_step delay for each move
 */
void Head::sweepLeft(const double &tiltAngle_offset, const double &panAngle_increment, const useconds_t delay_step)
{
    tilt = degree2raw(tiltAngle_offset, TILT_CONV);
    this->write(tilt);
    for (pan = HEAD_PAN_MIN; pan < HEAD_PAN_MAX; tilt += degree2raw(panAngle_increment, PAN_CONV)) {
        if (*flag_INT) break;
        usleep(delay_step);
        this->write(pan);
    }
}

/*!
 * \brief move form right to left
 * \param tiltAngle_offset set \p y position of head
 * \param panAngle_increment how many degree for each move along \p x axis
 * \param delay_step delay for each move
 */
void Head::sweepRight(const double &tiltAngle_offset, const double &panAngle_increment, const useconds_t delay_step)
{
    tilt = degree2raw(tiltAngle_offset, TILT_CONV);
    this->write(tilt);
    for (pan = HEAD_PAN_MIN; pan <= HEAD_PAN_MAX; pan += degree2raw(panAngle_increment, PAN_CONV)) {
        if (*flag_INT) break;
        usleep(delay_step);
        this->write(pan);
    }
}

/*!
 * \brief Nodding once
 * \param tiltAngle_offset set \p x position of head
 * \param panAngle_increment how many degree for each move along \p y axis
 * \param delay_steps delay for each move
 */
void Head::nod(const double &panAngle_offset, const double &tiltAngle_increment, const useconds_t delay_steps)
{
    pan = degree2raw(panAngle_offset, PAN_CONV);
    this->write(pan);
    for (tilt = HEAD_TILT_MAX; tilt >= HEAD_TILT_MIN; tilt -= degree2raw(tiltAngle_increment, TILT_CONV)) {
        if (*flag_INT) break;
        usleep(delay_steps);
        this->write(tilt);
    }
    for (tilt = HEAD_TILT_MIN; tilt <= HEAD_TILT_MAX; tilt += degree2raw(tiltAngle_increment, TILT_CONV)) {
        if (*flag_INT) break;
        usleep(delay_steps);
        this->write(tilt);
    }
}

/*!
 * \brief move along \p x axis
 * \param angle 0-360
 */
void Head::panning(const double angle)
{
    pan = degree2raw(angle, PAN_CONV);
    this->write(pan);
}

/*!
 * \brief move along \p y axis
 * \param angle 0-360
 */
void Head::tilting(const double angle)
{
    tilt = degree2raw(angle, TILT_CONV);
    this->write(tilt);
}

/*!
 * \brief move to specific angle position
 * \param tiltAngle
 * \param panAngle
 */
void Head::moveAtAngle(const double tiltAngle, const double panAngle)
{
    pan = degree2raw(panAngle, PAN_CONV);
    this->write(pan);
    tilt = degree2raw(tiltAngle, TILT_CONV);
    this->write(tilt);
}

/*!
 * \brief move to specific angle position like graph of cosinus
 * \param degree
 * \param multiplier
 */
void Head::cosinusMove(const double degree, const int multiplier)
{
    pan = degree2raw(degree, PAN_CONV);
    this->write(pan);
    tilt = ((cos(degree*M_PI/180)*multiplier+1) / (abs(multiplier))+1) + (2*HEAD_TILT_MIN);
    this->write(tilt);
}

/*!
 * \brief load configuration data in *.ini file.
 *
 * \param ini
 * \param section
 */
void Head::loadINI(minIni *ini, const string &section)
{
    P_gain.pan = ini->getd(section, "pan_P_gain");
    D_gain.pan = ini->getd(section, "pan_D_gain");
    P_gain.tilt = ini->getd(section, "tilt_P_gain");
    D_gain.tilt = ini->getd(section, "tilt_D_gain");

    P_noise.frame_x = ini->getd(section, "x_Prediction_Noise");
    P_noise.frame_y = ini->getd(section, "y_Prediction_Noise");
    K_noise.frame_x = ini->getd(section, "x_Kalman_Noise");
    K_noise.frame_y = ini->getd(section, "y_Kalman_Noise");

    dt = ini->getd(section, "Period");
    RC = ini->getd(section, "Time_Constant");
}

/*!
 * \brief save configuration data into *.ini file.
 *
 * \param ini
 * \param section
 */
void Head::saveINI(minIni *ini, const string &section)
{
    ini->put(section, "pan_P_gain", P_gain.pan);
    ini->put(section, "pan_D_gain", D_gain.pan);
    ini->put(section, "tilt_P_gain", P_gain.tilt);
    ini->put(section, "tilt_D_gain", D_gain.tilt);
}

/*!
 * \brief Enable data logging and save it under *.log extension.
 *
 *      The data that logged is form Head::targetTracking() function.
 * It consist : x, y, area. The separator of the data is <TAB> white space.
 * You can plot the log data in spradsheet by importing it.
 *
 * \param filename  must be *.ini extension
 * \param each_seconds  log data for each second
 * \sa Head::targetTracking()
 */
void Head::logDataEnable(const string &filename, const double &each_seconds)
{
    log = new LogFile(filename);
    time_gap = each_seconds;
}

/*!
 * \brief connect to uGM_Cxx class controller
 * \param controller
 */
void Head::open(uGM_Cxx &controller)
{
    this->controller = &controller;
    controller_enable = true;
}


uint32_t Head::degree2raw(const double degree, bool type)
{
    if (type)     // tilt
        return (degree*raw_tilt_size/360)+HEAD_TILT_MIN;
    else          // pan
        return (degree*raw_pan_size/360)+HEAD_PAN_MIN;
}


double Head::kalmanFilter(double measured_val, const uint8_t iteration, const double Q, const double R, bool begin_measure)
{
    static double K, P;
    static double P_temp;
//    static uint16_t raw_temp_est;
//    static bool once;

    /* ambigu mau tak pke pa ga ya... ?
    // 1 0 1
    // do flip-flop for var once
    if (begin_measure && !(kalman_once)) {
        once = true;
        kalman_once = true;
    } else if (!(begin_measure) && kalman_once)
        kalman_once = false;

    // initialize once
    if (once) {
        raw_est_last = measured_val;
        once = false;
    }
    */

    if (begin_measure) {
        for (uint8_t i = 0; i < iteration; ++i) {
            // do prediction
            // raw_temp_est = raw_est_last;
            P_temp = P_last + Q;
            // calculate Kalman gain
            K = P_temp * (1/(P_temp + R));
            // correction
            raw_est = raw_est_last + K * (measured_val - raw_est_last);
            P = (1 - K) * P_temp;
            // update our last's
            P_last = P;
            raw_est_last = raw_est;
            // measure error but not use in correction
            error_kalman = raw_est - measured_val;
        }
        return raw_est;
    } else {
        P_last = 0;
        raw_est_last = 0;
        return measured_val;
    }
}


int Head::PIDControll(const double measured_val, const double ideal_val, const double Kp, const double Kd, const double Ki)
{
    static int P, I, D;

    err = ideal_val - measured_val;
    sumErr += lastErr;
    P = err*Kp;
    D = (err - lastErr)*Kd;
    I = sumErr*Ki;
    lastErr = err;

    return (P+I+D);
}


double Head::lowPassFilter(const double measured_val, const double dt, const double RC, bool begin_measure)
{
    static double alpha, result_val;

    if (lowPass_once) {
        alpha = dt / (dt + RC);
        if (alpha > 1) alpha = .99; // not 1, beacause {1-1 = 0}
        if (alpha < 0) alpha = 0;

        lowPass_once = false;
    }
    if (!begin_measure) result_val = measured_val;
    else result_val = (alpha*measured_val) + ((1-alpha)*result_val);
    return result_val;
}

void Head::write(uint32_t &command)
{
    if (controller_enable) {
        if (command == tilt)
            controller->writeWord(uGM_Cxx::HEAD_Y, command);
        if (command == pan)
            controller->writeWord(uGM_Cxx::HEAD_X, command);
    } else
        port_write(command);
}

/*!
 * \brief write log file
 */
void Head::logData()
{
    if (time_gap > 0) {
        static double dt;

        if (log_once) {
            log->logData("time");
            log->logData("PID err");
            log->logData("Kalman Err");
            log->logData("x");
            log->logData("y");
            log_once = false;
        }

        if ((dt += log->getdiffTime()) >= time_gap) {
            log->createLine();
            log->logData(err);
            log->logData(error_kalman);
            log->logData(pan);
            log->logData(tilt);
            dt = 0;
        }
    }
}
