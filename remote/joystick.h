/* Joystick Controll
 *
 * Author : Alfarobi
 *
 * Status : NOT Tested
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef __JOYSTICK_HH__
#define __JOYSTICK_HH__

#include <stdint-gcc.h>
#include <time.h>
#include <string>
#include <vector>
#include <linux/joystick.h>

#define JS_EVENT_BUTTON 0x01    /*!< button pressed/released. */
#define JS_EVENT_AXIS   0x02    /*!< joystick moved. */
#define JS_EVENT_INIT   0x80    /*!< initial state of device. */

namespace afr
{

/*!
 * \brief Encapsulates all data relevant to a sampled joystick event.
 */
class JoystickEvent
{
public:
  unsigned int time; /*!<  The timestamp of the event, in milliseconds.*/

  short value; /*!< The value associated with this joystick event.
                 For buttons this will be either 1 (down) or 0 (up).
                 For axes, this will range between -32768 and 32767. */

  unsigned char type; /*!< The event type. */
  unsigned char number; /*!< The axis/button number.*/

  /*!
   * \return \c true if this event is the result of a button press.
   */
  bool isButton()
  {
    return (type & JS_EVENT_BUTTON) != 0;
  }

  /*!
   * \return \c true if this event is the result of an axis movement.
   */
  bool isAxis()
  {
    return (type & JS_EVENT_AXIS) != 0;
  }

  /*!
   * \return \c true if this event is part of the initial state obtained when
   * the joystick is first connected to.
   */
  bool isInitialState()
  {
    return (type & JS_EVENT_INIT) != 0;
  }
};

/*!
 * \brief Represents a joystick device. Allows data to be sampled from it.
 */
class Joystick
{
private:
  void openPath(const std::string &devicePath);
  
  int _fd;
  
public:
  ~Joystick();

  Joystick();
  Joystick(int joystickNumber);
  Joystick(const std::string &devicePath);
 
  bool isFound();
  bool sample(JoystickEvent* event);
};

/*!
 * \brief Add some combo movement like you play in the game for example Playstation 2
 */
class JoystickCombo : public JoystickEvent
{
private:
    Joystick *dev;

    struct Button {unsigned char number; short value;};
    bool process(std::vector<Button> &b, time_t s_timeOut, uint8_t type);
    time_t diffTime(const timespec &start_s);

public :
    void open(const std::string &dev);
    void open(Joystick &device);

    bool button(time_t timeOut_seconds, int nCombo, ...);
    bool axis(time_t timeOut_seconds, int nCombo, ...);
};

}

#endif
