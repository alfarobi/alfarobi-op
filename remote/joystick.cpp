/* Joystick Controll
 *
 * Author : Alfarobi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "joystick.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <stdarg.h>
#include <string>
#include <sstream>

using namespace afr;

/*!
 * Initialises an instance for the first joystick: /dev/input/js0
 */
Joystick::Joystick()
{
    openPath("/dev/input/js0");
}

/*!
 * Initialises an instance for the joystick with the specified,
 * zero-indexed number.
 */
Joystick::Joystick(int joystickNumber)
{
    std::stringstream sstm;
    sstm << "/dev/input/js" << joystickNumber;
    openPath(sstm.str());
}

/*!
 * Initialises an instance for the joystick device specified.
 */
Joystick::Joystick(const std::string &devicePath)
{
    openPath(devicePath);
}

void Joystick::openPath(const std::string &devicePath)
{
    _fd = open(devicePath.c_str(), O_RDONLY | O_NONBLOCK);
}

/*!
 * \brief Attempts to populate the provided JoystickEvent instance with data
 * from the joystick.
 * \return \c true if data is available, otherwise \c false.
 */
bool Joystick::sample(JoystickEvent* event)
{
    int bytes = read(_fd, event, sizeof(*event));

    if (bytes == -1)
        return false;

    // NOTE if this condition is not met, we're probably out of sync and this
    // Joystick instance is likely unusable
    return bytes == sizeof(*event);
}

/*!
 * \return \c true if the joystick was found and may be used, otherwise \c false.
 */
bool Joystick::isFound()
{
    return _fd >= 0;
}

Joystick::~Joystick()
{
    close(_fd);
}


bool JoystickCombo::process(std::vector<Button> &b, time_t s_timeOut, uint8_t type)
{
    timespec start;
    JoystickEvent data;

    clock_gettime(CLOCK_MONOTONIC, &start);

    for (uint i = 0; i < b.size();) {
        if (s_timeOut > diffTime(start)) return false;

        if ( ! dev->sample(&data)) continue;

        if ( (data.type & type) != 0 ) {
            if (data.number != b[i].number && data.value != b[i].value) continue;
            i++;
        }
    }

    return true;
}

time_t JoystickCombo::diffTime(const timespec &start_s)
{
    static timespec res_s, end_s;
    clock_gettime(CLOCK_MONOTONIC, &end_s);
    do {
        res_s.tv_sec = end_s.tv_sec - start_s.tv_sec;
        res_s.tv_nsec = end_s.tv_nsec - start_s.tv_nsec;
        if (res_s.tv_nsec < 0) {
            --res_s.tv_sec;
            res_s.tv_nsec += 1000000000;
        }
    } while (0);
    return (res_s.tv_sec + res_s.tv_nsec/(CLOCKS_PER_SEC*1000));
}

/*!
 * \brief open / access joystick device file
 * \param dev
 */
void JoystickCombo::open(const std::string &dev)
{
    this->dev = new Joystick(dev);
}

/*!
 * \brief set the joystick that you want to listen
 * \param device
 */
void JoystickCombo::open(Joystick &device)
{
    dev = &device;
}

/*!
 * \brief set the combination button and read the joystick if pressed button is exactly as the combination
 * \param timeOut_seconds
 *      define timeout
 * \param nCombo
 *      number of involve button for combination
 * \param after 2nd arg
 *      button id number
 * \param after 3rd arg
 *      button value is up (1) or down (0)
 * \return \c true if the combination is correct, otherwise \c false
 */
bool JoystickCombo::button(time_t timeOut_seconds, int nCombo, ...)
{
    va_list arg;
    std::vector<Button> button;

    va_start(arg, nCombo);
    for (int i = 0; i < nCombo; ++i) {
            button[i].number = (uint8_t) va_arg(arg, int);
            button[i].value = (short) va_arg(arg, int);
    }
    va_end(arg);

    return process(button, timeOut_seconds, JS_EVENT_BUTTON);
}

/*!
 * \brief set the combination axis and read the joystick if moved axis is exactly as the combination
 * \param timeOut_seconds
 *      define timeout
 * \param nCombo
 *      number of involve button for combination
 * \param after 2nd arg
 *      axis id number (originally joystick PS/2 have 2 analog axis)
 * \param after 3rd arg
 *      axis value as int
 * \return \c true if the combination is correct, otherwise \c false
 */
bool JoystickCombo::axis(time_t timeOut_seconds, int nCombo, ...)
{
    va_list arg;
    std::vector<Button> button;

    va_start(arg, nCombo);
    for (int i = 0; i < nCombo; ++i) {
            button[i].number = (uint8_t) va_arg(arg, int);
            button[i].value = (short) va_arg(arg, int);
    }
    va_end(arg);

    return process(button, timeOut_seconds, JS_EVENT_AXIS);
}
