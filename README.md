Alfarobi-Framework
==================

Framework Feature :
---------------------
    Data Logging
    Load/Save File Configuration (for calibration purpose)
    Function for set Interrupt condition (useful for multithreading)


Requirements :
------------------

Tools :

    build-essetntials
    qmake (optional)
    
Library :

    >=OpenCV 2.0
    libserial
    libpthread
    darwin library (but it's included in this framework {just motion though :D})
    
    
Recommended Tools for Development :
-----------------------------------
    Qt-Creator (not necceseary but recommended because its use .pro file)
    Roboplus
    Darwin Robotis Framework
    git (optional for contributing development source code)    
    
    
In-Progress :
-----------------------------
    Calibration Example
    Some (advance) Image Processing module
    GUI for calibration & controlling robot (maybe)
    Template Code for contributing (maybe) (its in template code.tar.gz)
