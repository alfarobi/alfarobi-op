/* Visual Odometry
 *
 * Author : Alfarobi
 *
 * Status : NOT Finished
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef VISUALODOMETRY_H
#define VISUALODOMETRY_H

#include <stdint-gcc.h>
#include <time.h>
#include "../std/minIni/minIni.h"
#include "../std/logfile.h"
#include "../std/speed_test.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>

namespace afr {

class VisualOdometry
{
public:
    VisualOdometry();

    void setParam(const int &max_corner, const cv::Size &winsize);
    void setHorizon(const float &min, const float &max);
    void setThreshold(const float &min_diff, const float &max_diff); // set threshold biar ga terlalu sensitif terhadap gerakan
    void setHeading(const double &heading);
    void setPoint(const cv::Point2d &position);

    void process(const cv::Mat &inputGrayImg, const timespec &time_cap_frame);

    double getHeading();
    cv::Point2d getVelocity();
    cv::Point2d getDisplacement(const double &distance_cam_to_ground, const double &pitch_cam);
    cv::Point2d getTranslation(const double &distance_cam_to_ground, const double &pitch_cam);
    void draw(cv::Mat *inputImage);

private:
    cv::Mat last_grayImg;
    cv::TermCriteria termcrit;
    std::vector<cv::Point2f> corners, last_corners;
    std::vector<uchar> statuses;
    std::vector<float> errs;

    cv::Size winsize;
    int max_corner;

    struct DiffTrack { std::vector<float> dx, dy; };
    DiffTrack ground, sky;
    double dt;
    float x_sum, y_sum;
    float min_horizon, max_horizon;
    float min_diff, max_diff;
    timespec last_time_cap, time_cap;

    uint fx, fy;
    double heading;
    cv::Point2d position;

    // daleman process();
    void opticalFlow_init(const cv::Mat &inputGrayImage, const int &max_corner, bool *init_state);
    void opticalFlow_init(const cv::Mat &inputGrayImage, const int &max_corner);
    void opticalFlow_proccess(const cv::Mat &inputGrayImage);

    double getRotationIncrement(const cv::Point2d &diff);

    void calcDiffTrack();       // kalkulasi selisih corners
    cv::Point2d getMedian(DiffTrack &horizon);    // ambil median data dari selisih
    cv::Point2d getMean(const DiffTrack &horizon);      // ambil rata2 data dari selisih
};

}

#endif // VISUALODOMETRY_H
