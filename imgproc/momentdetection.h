/* Moment Detection
 *
 * Author : Alfarobi
 *
 * Status : Tested
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef MOMENTDETECTION_H
#define MOMENTDETECTION_H

#include <stdint-gcc.h>
#include "../std/minIni/minIni.h"
#include "../std/logfile.h"
#include <opencv2/imgproc/imgproc.hpp>

#define LUT_SIZE 16777216

#define between(min,var,max)    (min <= var && var <= max)

namespace afr {

/*!
 * \brief Class for doing what neccesery to do moment detection
 */
class MomentDetection
{
public:
    MomentDetection();

    void loadINISettings(minIni *ini, const std::string &section);
    void saveINISettings(minIni *ini, const std::string &section);

    void setHSVmin(const uint8_t &H, const uint8_t &S, const uint8_t &V);
    void setHSVmax(const uint8_t &H, const uint8_t &S, const uint8_t &V);

    void generateLUT_HSV();
    void threshold(cv::Mat *InputOutputImage, const bool inverse_binary = false);
    void moment(const cv::Mat &InputImage);
    void fillInteriors(cv::Mat *inputOutputImage, int holeLevel = 2);

    void logDataEnable(const std::string &filename, const double &each_seconds);
    void draw(const cv::Mat &inputSrcImage, cv::Mat *outputDstImage);

    double getX() const { return x; }
    double getY() const { return y; }
    double getArea() const { return area; }

private:
    std::vector< std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;

    cv::Moments moments;
    double x, y, area;
    bool moment_is_called;

    struct MinMax { uint8_t min, max; };
    struct HSV { struct MinMax H, S, V; } HSVColor;

    bool hsvLutFilled;
    struct HueSatVal { uchar h, s, v; } hsvLut[LUT_SIZE];
    cv::Mat temp;
    uint idx;

    LogFile *log;
    double time_gap;

    void logData();
    void fastBGR2HSVfilter(const cv::Mat InputImage,
                           const cv::Scalar min, const cv::Scalar max, cv::Mat *OutputImage,
                           const bool inverse = false);
};

}

#endif // MOMENTDETECTION_H
