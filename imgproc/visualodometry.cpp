/* Visual Odometry
 *
 * Author : Alfarobi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "visualodometry.h"

using namespace afr;

VisualOdometry::VisualOdometry()
{
    termcrit = cv::TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03);
}

void VisualOdometry::setParam(const int &max_corner, const cv::Size &winsize)
{
    this->max_corner = max_corner; this->winsize = winsize;
}

void VisualOdometry::opticalFlow_init(const cv::Mat &inputGrayImage, const int &max_corner, bool *init_state)
{
    if (*init_state) {
        cv::goodFeaturesToTrack(inputGrayImage, corners, max_corner, 0.01, 10);
        cv::cornerSubPix(inputGrayImage, corners, winsize, cv::Size(-1,-1), termcrit);
        *init_state = false;
    }
}

bool once_init = true;
void VisualOdometry::opticalFlow_init(const cv::Mat &inputGrayImage, const int &max_corner)
{
    opticalFlow_init(inputGrayImage, max_corner, &once_init);
}

void VisualOdometry::opticalFlow_proccess(const cv::Mat &inputGrayImage)
{
    if (!last_corners.empty())
        cv::calcOpticalFlowPyrLK(last_grayImg, inputGrayImage, last_corners, corners, statuses, errs, winsize, 3, termcrit);
    inputGrayImage.copyTo(last_grayImg); // TODO : safe timer
}

double VisualOdometry::getRotationIncrement(const cv::Point2d &diff)
{
    return atan2(diff.x, fx);
}

void VisualOdometry::calcDiffTrack()
{
    ground.dx.clear(); ground.dy.clear();
    sky.dx.clear(); sky.dy.clear();
    x_sum = 0; y_sum = 0;

    if ( !(last_corners.empty() || corners.empty() || (corners.size() != last_corners.size())) ) {
        static float dx, dy, diff;

        for (size_t i = 0, cx = 0, cy = 0; i < corners.size(); ++i) {
            dx = ((corners[i].x)-(last_corners[i].x));
            dy = ((corners[i].y)-(last_corners[i].y));

            diff = sqrt(dx*dx + dy*dy);
            if (min_diff <= diff && diff <= max_diff) { // threshold

                // insert difference of point above horizon
                if ( (corners[i].y > max_horizon) && (last_corners[i].y > max_horizon) ) {
                    if (dx != 0) { sky.dx.insert(sky.dx.begin()+cx, dx); cx++; }
                    if (dy != 0) { sky.dy.insert(sky.dy.begin()+cy, dy); cy++; }
                }

                // insert difference of point below horizon
                if ( (corners[i].y < min_horizon) && (last_corners[i].y < min_horizon) ) {
                    if (dx != 0) { ground.dx.insert(ground.dx.begin()+cx, dx); cx++; }
                    if (dy != 0) { ground.dy.insert(ground.dy.begin()+cy, dy); cy++; }
                }
            }

            // sum all difference points
            x_sum += dx;
            y_sum += dy;
        }
    }
    // save last data for further tracking
    last_corners = corners;
}

cv::Point2d VisualOdometry::getMedian(DiffTrack &horizon)
{
    std::sort(horizon.dx.begin(), horizon.dx.end());
    std::sort(horizon.dy.begin(), horizon.dy.end());
    return cv::Point2d(horizon.dx.at(horizon.dx.size()/2), horizon.dy.at(horizon.dy.size()/2));
}

cv::Point2d VisualOdometry::getMean(const DiffTrack &horizon)
{
    return cv::Point2d(x_sum/horizon.dx.size(), y_sum/horizon.dy.size());
}


double VisualOdometry::getHeading()
{
    if (0 < heading && heading < 360)
        heading += getRotationIncrement(getMedian(sky));
    else if (heading < 0)
        heading = 360 - fabs(heading - getRotationIncrement(getMedian(sky)));
    else if (heading > 360)
        heading = getRotationIncrement(getMedian(sky));

    return heading;
}

cv::Point2d VisualOdometry::getVelocity()
{
    static cv::Point2f v;
    v = getMedian(ground);
    return (cv::Point2d(v.x/dt, v.y/dt));
}

cv::Point2d VisualOdometry::getDisplacement(const double &distance_cam_to_ground, const double &pitch_cam)
{
    static double Z; // distance lens camera to ground (diagonaly) r = sqrt(x^2 + y^2);
    static cv::Point2f diff;

    Z = distance_cam_to_ground/cos(pitch_cam);
    diff = getMedian(ground);
    diff.x = diff.x*Z/fx;
    diff.y = diff.y*Z/fy;
    return diff;
}

cv::Point2d VisualOdometry::getTranslation(const double &distance_cam_to_ground, const double &pitch_cam)
{
    static cv::Point2d point_displacement;
    point_displacement = getDisplacement(distance_cam_to_ground, pitch_cam);
    position.x += point_displacement.x*cos(getHeading());
    position.y += point_displacement.x*sin(getHeading());
    return position;
}

void VisualOdometry::draw(cv::Mat *inputImage)
{
    if (!last_corners.empty())
        for (size_t i = 0; i < corners.size(); ++i) {
            cv::circle(*inputImage, corners[i], 1, CV_RGB(0,0,255), CV_FILLED);
            if (statuses[i]) cv::line(*inputImage, corners[i], last_corners[i], CV_RGB(255,255,0));
        }
}

void VisualOdometry::setHeading(const double &heading)
{
    this->heading = heading;
}

void VisualOdometry::setPoint(const cv::Point2d &position)
{
    this->position = position;
}

void VisualOdometry::process(const cv::Mat &inputGrayImg, const timespec &time_cap_frame)
{
    opticalFlow_init(inputGrayImg, max_corner);
    opticalFlow_proccess(inputGrayImg);
    calcDiffTrack();
    if (!last_corners.empty())
        dt = speedMeasure(time_cap_frame, last_time_cap);
    last_time_cap = time_cap_frame;
}
