/* Circle Detection
 *
 * Author : Alfarobi
 *
 * Status : Stil have some bug. Need to be fixed
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#ifndef CIRCLEDETECTION_H
#define CIRCLEDETECTION_H

#include "stdint-gcc.h"
#include "../std/minIni/minIni.h"
#include "../std/logfile.h"
#include <opencv2/imgproc/imgproc.hpp>

namespace afr {

class CircleDetection
{
public:
    CircleDetection();

    void loadINISettings(minIni *ini, const std::string &section);
    void saveINISettings(minIni *ini, const std::string &section);
    void setHoughParam(double inverseRatioResolution_of_image, double minDist_of_detectedCircle,
                       double threshCannyEdge, double threshCenterDetection,
                       int radiusMin, int radiusMax);

    void logDataEnable(std::string &filename, const double &each_seconds);

    void threshold(cv::Mat *InputOutputImage);
    int houghTransform(cv::Mat *InputOutputImage);
    void draw(cv::Mat *inputOutputImage);

    double getX() const { return x; }
    double getY() const { return y; }
    double getRadius() const { return radius; }
    cv::Vec3f getCircle(const uint &idx) { return circles[idx]; }

private:
    std::vector<cv::Vec3f> circles;
    double inverseRatioResolution_of_image, minDist_of_detectedCircle, threshCannyEdge, threshCenterDetection;
    int radiusMin, radiusMax;
    float x, y, radius;

    LogFile *log;
    double time_gap;

    void logData();
};

}

#endif // CIRCLEDETECTION_H
