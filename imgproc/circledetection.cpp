/* Circle Detection
 *
 * Author : Alfarobi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "circledetection.h"

using namespace afr;

static bool log_once = true;


CircleDetection::CircleDetection()
{
    time_gap = -1;
    inverseRatioResolution_of_image = 1;
    minDist_of_detectedCircle = 1;
    threshCannyEdge = 1;
    threshCenterDetection = 1;
    radiusMin = 0;
    radiusMax = 0;
}

/*!
 * \brief load configuration data in *.ini file.
 *
 * \param ini
 * \param section
 * \sa CircleDetection::saveINISettings()
 */
void CircleDetection::loadINISettings(minIni *ini, const std::string &section)
{
    inverseRatioResolution_of_image = ini->getd(section, "inverse_ratio_resolution_of_image_(dp)");
    minDist_of_detectedCircle = ini->getd(section, "minimum_distribution_of_detected_circle");
    threshCannyEdge = ini->getd(section, "threshold_parameter_CannyEdge");
    threshCenterDetection = ini->getd(section, "threshold_parameter_CenterDetection");
    radiusMin = ini->geti(section, "min_radius");
    radiusMax = ini->geti(section, "max_radius");
}

/*!
 * \brief save configuration data into *.ini file.
 *
 * \param ini
 * \param section
 * \sa CircleDetection::loadINISettings()
 */
void CircleDetection::saveINISettings(minIni *ini, const std::string &section)
{
    ini->put(section, "inverse_ratio_resolution_of_image_(dp)", inverseRatioResolution_of_image);
    ini->put(section, "minimum_distribution_of_detected_circle", minDist_of_detectedCircle);
    ini->put(section, "threshold_parameter_CannyEdge", threshCannyEdge);
    ini->put(section, "threshold_parameter_CenterDetection", threshCenterDetection);
    ini->put(section, "min_radius", radiusMin);
    ini->put(section, "max_radius", radiusMax);
}

/*!
 * \brief set parameter that needed for \i hough \i transform method
 * \param inverseRatioResolution_of_image
 * \param minDist_of_detectedCircle
 * \param threshCannyEdge
 * \param threshCenterDetection
 * \param radiusMin
 * \param radiusMax
 * \sa CircleDetection::houghTransform()
 */
void CircleDetection::setHoughParam(double inverseRatioResolution_of_image, double minDist_of_detectedCircle, double threshCannyEdge, double threshCenterDetection, int radiusMin, int radiusMax)
{
    this->inverseRatioResolution_of_image = inverseRatioResolution_of_image;
    this->minDist_of_detectedCircle = minDist_of_detectedCircle;
    this->threshCannyEdge = threshCannyEdge;
    this->threshCenterDetection = threshCenterDetection;
    this->radiusMin = radiusMin;
    this->radiusMax = radiusMax;
}

/*!
 * \brief Enable data logging and save it under *.log extension.
 *
 *      The data that logged is form CircleDetection::houghTransform() function.
 * It consist : x, y, area. The separator of the data is <TAB> white space.
 * You can plot the log data in spradsheet by importing it.
 *
 * \param filename  must be *.ini extension
 * \param each_seconds  log data for each second
 * \sa CircleDetection::houghTransform()
 */
void CircleDetection::logDataEnable(std::string &filename, const double &each_seconds)
{
    log = new LogFile(filename);
    time_gap = each_seconds;
}

/*!
 * \brief just convert the color to gray
 * \param InputOutputImage
 */
void CircleDetection::threshold(cv::Mat *InputOutputImage)
{
    cv::cvtColor(*InputOutputImage, *InputOutputImage, CV_BGR2GRAY);
    //    cv::Laplacian(*InputOutputImage, *InputOutputImage, CV_8U);
    //    cv::equalizeHist(*InputOutputImage, *InputOutputImage);
    //    cv::threshold(*InputOutputImage, *InputOutputImage, minval, maxval, CV_THRESH_OTSU);
}

/*!
 * \brief do circle detection with \i hough \i transform method
 * \param InputOutputImage
 * \return number of detected circle
 * \sa CircleDetection::setHoughParam()
 */
int CircleDetection::houghTransform(cv::Mat *InputOutputImage)
{
    radius = 0;

    // Guard
    if (inverseRatioResolution_of_image < 1 || minDist_of_detectedCircle < 1 || threshCannyEdge < 1 || threshCenterDetection < 1) {
        inverseRatioResolution_of_image = 1;
        minDist_of_detectedCircle = 1;
        threshCannyEdge = 1;
        threshCenterDetection = 1;
    }

    cv::HoughCircles(*InputOutputImage, circles, CV_HOUGH_GRADIENT, inverseRatioResolution_of_image, minDist_of_detectedCircle, threshCannyEdge, threshCenterDetection, radiusMin, radiusMax);
    for (uint i = 0; i < circles.size(); ++i) {
        if (circles[i][2] > radius) {
            radius = circles[i][2];
            x = circles[i][0];
            y = circles[i][1];
        }
    }

    // logging data
    logData();

    return circles.size();
}

/*!
 * \brief draw the result after proccessing it.
 *
 * \param inputSrcImage
 * \param outputDstImage
 */
void CircleDetection::draw(cv::Mat *inputOutputImage)
{
    for (size_t i = 0; i < circles.size(); ++i) {
        cv::circle(*inputOutputImage, cv::Point(cvRound(circles[i][0]), cvRound(circles[i][1])), cvRound(circles[i][2]), cv::Scalar::all(255), 2);
    }
    if (!circles.empty())
        cv::circle(*inputOutputImage, cv::Point(cvRound(x), cvRound(y)), cvRound(radius), cv::Scalar::all(175), 3);
}

/*!
 * \brief write log file
 */
void CircleDetection::logData()
{
    if (time_gap > 0) {
        static double dt;

        if (log_once) {
            log->logData("time");
            log->logData("x");
            log->logData("y");
            log->logData("radius");
            log_once = false;
        }

        if ((dt += log->getdiffTime()) >= time_gap) {
            log->createLine(); // it will print the time
            log->logData(x);
            log->logData(y);
            log->logData(radius);
            dt = 0;
        }
    }
}

