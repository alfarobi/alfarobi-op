#include "cameracalibration.h"

using namespace afr;

CameraCalibration::CameraCalibration(const int device)
{
}


void CameraCalibration::calibrate(const cv::Mat &inputImg, cv::Point2d aperture)
{
//    cv::calibrateCamera(objectPoints, imagePoints,
//                        cv::Size(inputImg.cols, inputImg.rows),
//                        cameraMatrix, distCoeffs,
//                        rvecs, tvecs,
//                        CV_CALIB_FIX_PRINCIPAL_POINT);
    cameraMatrix = cv::getOptimalNewCameraMatrix(cameraMatrix, distCoeffs,
                                                 cv::Size(inputImg.cols, inputImg.rows),
                                                 .1);
    cv::calibrationMatrixValues(cameraMatrix,
                                cv::Size(inputImg.cols, inputImg.rows),
                                aperture.x, aperture.y,
                                fovx, fovy,
                                focal_length, principal_point, aspect_ratio);
}
