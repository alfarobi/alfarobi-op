/* Moment Detection
 *
 * Author : Alfarobi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

#include "momentdetection.h"
#include <iostream>

// WARNING : use cv::Exception or not ?

using namespace afr;

cv::Mat rgb, hsv;
static bool log_once = true;


MomentDetection::MomentDetection()
{
    hsvLutFilled = false;
    moment_is_called = false;
    x = 0, y = 0;
    time_gap = -1;
    rgb.create(cv::Size(256,256), CV_8UC3);
    hsv.create(cv::Size(256,256), CV_8UC3);
}

/*!
 * \brief load configuration data in *.ini file.
 *
 * \param ini
 * \param section
 */
void MomentDetection::loadINISettings(minIni *ini, const std::string &section)
{
    setHSVmin(ini->geti(section, "hue_min"), ini->geti(section, "saturation_min"), ini->geti(section, "value_min"));
    setHSVmax(ini->geti(section, "hue_max"), ini->geti(section, "saturation_max"), ini->geti(section, "value_max"));
}

/*!
 * \brief save configuration data into *.ini file.
 *
 * \param ini
 * \param section
 */
void MomentDetection::saveINISettings(minIni *ini, const std::string &section)
{
    ini->put(section, "hue_min", HSVColor.H.min);
    ini->put(section, "hue_max", HSVColor.H.max);
    ini->put(section, "saturation_min", HSVColor.S.min);
    ini->put(section, "saturation_max", HSVColor.S.max);
    ini->put(section, "value_min", HSVColor.V.min);
    ini->put(section, "value_max", HSVColor.V.max);
}

/*!
 * \brief Enable data logging and save it under *.log extension.
 *
 *      The data that logged is form MomentDetection::moment() function.
 * It consist : x, y, area. The separator of the data is <TAB> white space.
 * You can plot the log data in spradsheet by importing it.
 *
 * \param filename  must be *.ini extension
 * \param each_seconds  log data for each second
 * \sa MomentDetection::moment()
 */
void MomentDetection::logDataEnable(const std::string &filename, const double &each_seconds)
{
    log = new LogFile(filename);
    time_gap = each_seconds;
}

/*!
 * \brief write log file
 */
void MomentDetection::logData()
{
    if (time_gap > 0) {
        static double dt;

        if (log_once) {
            log->logData("time");
            log->logData("x");
            log->logData("y");
            log->logData("area");
            log_once = false;
        }

        if ((dt += log->getdiffTime()) >= time_gap) {
            log->createLine(); // it will print the time
            log->logData(x);
            log->logData(y);
            log->logData(area);
            dt = 0;
        }
    }
}

/*!
 * \brief draw the result after proccessing it.
 *
 * \param inputSrcImage
 * \param outputDstImage
 */
void MomentDetection::draw(const cv::Mat &inputSrcImage, cv::Mat *outputDstImage)
{
    cv::cvtColor(*outputDstImage, *outputDstImage, CV_GRAY2BGR);
    *outputDstImage &= inputSrcImage;
    if (moment_is_called) {
        cv::circle(*outputDstImage, cv::Point(x, y), area/(10000000), CV_RGB(0,255,255), 2);
        moment_is_called = false;
    }
}

/*!
 * \param H
 * \param S
 * \param V
 */
void MomentDetection::setHSVmin(const uint8_t &H, const uint8_t &S, const uint8_t &V)
{
    HSVColor.H.min = H;
    HSVColor.S.min = S;
    HSVColor.V.min = V;
}

/*!
 * \param H
 * \param S
 * \param V
 */
void MomentDetection::setHSVmax(const uint8_t &H, const uint8_t &S, const uint8_t &V)
{
    HSVColor.H.max = H;
    HSVColor.S.max = S;
    HSVColor.V.max = V;
}

/*!
 * \brief generate Look Up Table form BGR to HSV.
 *
 */
void MomentDetection::generateLUT_HSV()
{
    // red & green fill
    for (int i = 0; i < 256; ++i) {
        for (int j = 0; j < 256; ++j) {
            rgb.ptr<cv::Vec3b>(i)[j][2] = (uchar) i;
            rgb.ptr<cv::Vec3b>(i)[j][1] = (uchar) j;
        }
    }

    for (int k = 0; k < 256; ++k) {
        // blue color loop
        for (int i = 0; i < 256; ++i) {
            for (int j = 0; j < 256; ++j) {
                rgb.ptr<cv::Vec3b>(i)[j][0] = (uchar) k;
            }
        }
        cvtColor(rgb, hsv, CV_BGR2HSV);

        for (int i = 0; i < 256; ++i) {
            for (int j = 0; j < 256; ++j) {
                idx = (k*256+j)*256 + i;

                hsvLut[ idx ].h = hsv.ptr<cv::Vec3b>(i)[j][0];
                hsvLut[ idx ].s = hsv.ptr<cv::Vec3b>(i)[j][1];
                hsvLut[ idx ].v = hsv.ptr<cv::Vec3b>(i)[j][2];
            }
        }
    }
    hsvLutFilled = true;
}

/*!
 * \brief threshold the image form HSV that was set before.
 *
 *      Use MomentDetection::setHSVmin() and MomentDetection::setHSVmax() for threshold the image.
 * \n Example:
 *  \code
 *      MomentDetection moment;
 *      moment.setHSVmin(20,30,190);
 *      moment.setHSVmax(70,80,255);
 *  \endcode
 * If you generated Look Up Table by calling MomentDetection::generateLUT_HSV()
 * it will convert the image with LUT method, otherwise it use cv::cvtColor().
 *
 * \param InputOutputImage
 * \param inverse_binary
 *      if 'true' it will fill the thresholded color with black and the non-thresholded with black
 *      otherwise if 'false' just the opposite.
 */
void MomentDetection::threshold(cv::Mat *InputOutputImage, const bool inverse_binary)
{
    //    cv::equalizeHist(temp, *InputOutputImage); // the image must CV_8UC1 (8-bit 1 channel) (GRAY)
    if (hsvLutFilled) {
        fastBGR2HSVfilter(*InputOutputImage,
                          cv::Scalar(HSVColor.H.min, HSVColor.S.min, HSVColor.V.min),
                          cv::Scalar(HSVColor.H.max, HSVColor.S.max, HSVColor.V.max),
                          InputOutputImage, inverse_binary);
    } else {
        cv::cvtColor(*InputOutputImage, *InputOutputImage, CV_BGR2HSV);
        cv::inRange(*InputOutputImage,
                    cv::Scalar(HSVColor.H.min, HSVColor.S.min, HSVColor.V.min),
                    cv::Scalar(HSVColor.H.max, HSVColor.S.max, HSVColor.V.max),
                    *InputOutputImage);
        if (inverse_binary) cv::threshold(*InputOutputImage, *InputOutputImage, 0, 255, CV_THRESH_BINARY_INV);
    }
}

/*!
 * \brief calculate the moment of image.
 *
 * \param InputImage
 */
void MomentDetection::moment(const cv::Mat &InputImage)
{
    moments = cv::moments(InputImage);
    x = moments.m10 / moments.m00;
    y = moments.m01 / moments.m00;
    area = moments.m00;
    moment_is_called = true;

    logData();
}

/*!
 * \brief detect contour og image and fill it with white
 *
 * \param inputOutputImage
 * \param holeLevel max value is 3.
 *      2 for fill the outer contour or original hole (parent contour).
 *      3 for fill the hole / contour inside the contour (child contour).
 */
void MomentDetection::fillInteriors(cv::Mat *inputOutputImage, int holeLevel)
{
    cv::findContours(*inputOutputImage, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
    if (holeLevel < 0) cv::drawContours(*inputOutputImage, contours, -1, cv::Scalar::all(255), CV_FILLED);
    else {
        if (holeLevel > 3) holeLevel = 3;
        for (size_t i = 0; i < contours.size(); ++i) {
            if (hierarchy[i][holeLevel] != -1)
                cv::drawContours(*inputOutputImage, contours, i, cv::Scalar::all(255), CV_FILLED);
        }
    }
}

/*!
 * \param InputImage
 * \param min
 * \param max
 * \param OutputImage
 * \param inverse
 */
void MomentDetection::fastBGR2HSVfilter(const cv::Mat InputImage,
                                        const cv::Scalar min, const cv::Scalar max, cv::Mat *OutputImage,
                                        const bool inverse)
{
    OutputImage->create(InputImage.rows, InputImage.cols, cv::DataType<bool>::type);
    for (int i = 0; i < InputImage.rows; ++i) {
        for (int j = 0; j < InputImage.cols; ++j) {
            idx = InputImage.ptr<cv::Vec3b>(i)[j][0]*65536 + InputImage.ptr<cv::Vec3b>(i)[j][1]*256 + InputImage.ptr<cv::Vec3b>(i)[j][2];

            if (between(min[0], hsvLut[idx].h, max[0])
                    && between(min[1], hsvLut[idx].s, max[1])
                    && between(min[2], hsvLut[idx].v, max[2]))
                if (inverse) OutputImage->ptr(i)[j] = 0;
                else OutputImage->ptr(i)[j] = 0xff;
            else
                if (inverse) OutputImage->ptr(i)[j] = 0xff;
                else OutputImage->ptr(i)[j] = 0;
        }
    }
}
