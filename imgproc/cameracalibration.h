/*
 * TODO : CamereCalibration()
 * use cv::calibrateCamera() to get matrix camera
 * decode it using normal way and using cv::calibrationMatrixValues()
 */

#ifndef CAMERACALIBRATION_H
#define CAMERACALIBRATION_H

#include <string>

#include <opencv2/calib3d/calib3d.hpp>
#include "uvcdynctrl/cameracontroll.h"
#include "../std/logfile.h"
#include "../std/minIni/minIni.h"

namespace afr
{

class CameraCalibration : public CameraControll
{
public:
    CameraCalibration(const int device);

    void loadINISettings(minIni *ini, const std::string &section);
    void saveINISettings(minIni *ini, const std::string &section);

    void calibrate(const cv::Mat &inputImg, cv::Point2d aperture);
    void correctImg(cv::Mat *outputImg);

    void logDataEnable(const std::string &filename, const double &each_seconds);
    void draw(const cv::Mat &inputSrcImage, cv::Mat *outputDstImage);

private:
    LogFile *log;
    double time_gap;

    void logData();

    //  params cv::calibrateCamera()
    std::vector<std::vector<cv::Point3f> > objectPoints;
    std::vector<std::vector<cv::Point2f> > imagePoints;
    cv::Mat3f cameraMatrix;
    cv::Mat distCoeffs;
    std::vector<cv::Mat> rvecs,tvecs;

    // result
    double fovx,fovy;
    double focal_length,aspect_ratio;
    cv::Point2d principal_point;
};

}

#endif // CAMERACALIBRATION_H
